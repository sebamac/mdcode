#ifndef __TVEC_H_
#define __TVEC_H_

#include <stdexcept>
#include <cmath>
#include <utility>

using coordinate = double[3];

extern inline double mod2(const coordinate &a) noexcept
{
	return a[0]*a[0] + a[1]*a[1] + a[2]*a[2];
}
extern inline double mod(const coordinate &a) noexcept
{
	return sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
}
extern inline void fillCoordinate(coordinate &coord, const double& x, const double& y, const double& z) noexcept
{
	coord[0] = x; coord[1] = y; coord[2] = z;
}
extern inline void crossProduct(const coordinate &v1, const coordinate &v2, coordinate &v) noexcept
{
	v[0] = v1[1]*v2[2] - v1[2]*v2[1];
	v[1] = v1[2]*v2[0] - v1[0]*v2[2];
	v[2] = v1[0]*v2[1] - v1[1]*v2[0];
}

extern inline double distance(const coordinate &v1, const coordinate &v2) noexcept
{
	coordinate temp = {v1[0] - v2[0], v1[1] - v2[1], v1[2] - v2[2]};
	return mod(temp);
}

class tvec
{
private:
	size_t _size;
	coordinate *_end;
	
protected:
	coordinate *_vec;
	
public:
	typedef coordinate* iterator;
	
	tvec();
	tvec(const size_t& size);
	tvec(const tvec& v);
	tvec(tvec&& v) noexcept;
	virtual ~tvec();
	
	tvec& operator=(const tvec &v);
	tvec& operator=(tvec&& v) noexcept;
	
	inline iterator begin() noexcept { return _vec; }
	inline iterator end()   noexcept { return _end;}
	
	void clear(); //free up space!
	void reset() noexcept; //(FAST) set all values to zero
	void assign(const tvec &v); //re-allocate and assign
	void assign(tvec&& v) noexcept;//move content 
	void copy(const tvec &v) noexcept; //(FAST) copy all values: MUST HAVE SAME SIZE: DO NOT CHECK!!!
	void resize(const size_t& size); //re-allocate and set values to zero

	inline const size_t& size() const noexcept { return _size; }

	inline const double& x(const size_t& i) const noexcept { return _vec[i][0]; }
	inline const double& y(const size_t& i) const noexcept { return _vec[i][1]; }
	inline const double& z(const size_t& i) const noexcept { return _vec[i][2]; }
	inline double& x(const size_t& i) noexcept { return _vec[i][0]; }
	inline double& y(const size_t& i) noexcept { return _vec[i][1]; }
	inline double& z(const size_t& i) noexcept { return _vec[i][2]; }

	const coordinate& at(const size_t& i) const; //slower
	coordinate& at(const size_t& i);

	inline const coordinate& operator[](const size_t& i) const noexcept { return _vec[i]; }
	inline coordinate& operator[](const size_t& i) noexcept { return _vec[i]; }
	
	inline const coordinate* data() const noexcept { return _vec; }

	inline bool empty() const noexcept { return _size==0; }
};

#endif // __TVEC_H_
