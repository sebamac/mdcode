#ifndef __XYZ_FRAME_H_
#define __XYZ_FRAME_H_

#include <iostream>
#include <fstream>
#include <iomanip>
#include <limits>
#include <system_error>
#include <exception>
#include <utility>

#include "coordinates.hpp"
#include "bufferedWriter.hpp"

using namespace std;

class xyzFrame: public tvec
{
protected:
	string *aLabels;
	string _commentLine;

	virtual ostream& print(ostream& str) const;
	virtual istream& read(istream& str);

public:
	xyzFrame() noexcept;
	xyzFrame(const string& fileName);
	xyzFrame(const xyzFrame& fr);
	xyzFrame(xyzFrame&& fr) noexcept;
	virtual ~xyzFrame();
	
	xyzFrame& operator=(const xyzFrame& fr);
	xyzFrame& operator=(xyzFrame&& fr) noexcept;

	void open(const string& fileName);
	void write(const string& fileName);
	inline void write(const string& fileName, const string& comment) { _commentLine = comment; write(fileName); }

	inline operator bool() const noexcept { return !tvec::empty(); }
	inline const size_t& natoms() const noexcept { return tvec::size(); } // size() method from tvec is faster and it's equal
	inline const string* atomLabels() const noexcept { return aLabels; }
	inline const string& atomLabel(const size_t& i) const noexcept { return aLabels[i]; }
	inline const string& commentLine() const noexcept { return _commentLine; }
	inline const tvec&   coordinates() const { return dynamic_cast<const tvec&>(*this); }
	inline void commentLine(const string& cm) noexcept { _commentLine = cm; }

	friend inline istream& operator>>(istream& str, xyzFrame& frame) { return frame.read(str); }
	friend inline ostream& operator<<(ostream& str, const xyzFrame& frame) { return frame.print(str); }
};

class vxyzFrame: public xyzFrame
{
protected:
	tvec vvec;

	virtual ostream& print(ostream& str) const;
	virtual istream& read(istream& str);

public:
	vxyzFrame();
	vxyzFrame(const string& fileName);
	vxyzFrame(const xyzFrame& fr);
	vxyzFrame(const xyzFrame& fr, const tvec& v);
	vxyzFrame(const vxyzFrame& fr);
	vxyzFrame(xyzFrame&&  fr) noexcept;
	vxyzFrame(vxyzFrame&& fr) noexcept;
	virtual ~vxyzFrame() = default;

	vxyzFrame& operator=(const vxyzFrame& fr);
	vxyzFrame& operator=(vxyzFrame&& fr) noexcept;

	vxyzFrame& operator=(const xyzFrame& fr);
	vxyzFrame& operator=(xyzFrame&& fr) noexcept;

	inline const tvec& velocities() const noexcept { return vvec; }
	inline tvec& velocities() noexcept { return vvec; }
};

#endif //__XYZ_FRAME_H_
