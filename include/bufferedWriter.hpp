#ifndef _BUFFERED_WRITER_H_
#define _BUFFERED_WRITER_H_

#include <fstream>
#include <string>
#include <thread>
#include <sstream>

using namespace std;

class bufferedWriter
{
private:
	bufferedWriter() noexcept;
	
	size_t _size;
	string _fileName;
	
	string *_beg, *_end, *_current; //end is end+1; end is deferentiable nor end+1
	string *_cp;
	
	std::thread _writeThread;
	void write();
	void writeThread();
	
public:
	bufferedWriter(const size_t& s, const string& name, const bool& append = false);
	bufferedWriter(const size_t& s, const string& name, const string& firstLine, const bool& append = false);
	bufferedWriter(const bufferedWriter& ) = delete;
	bufferedWriter(bufferedWriter&& bufWrt) noexcept;
	bufferedWriter& operator=(const bufferedWriter& ) = delete;
	bufferedWriter& operator=(bufferedWriter&& bufWrt) noexcept;
	virtual ~bufferedWriter();

	bufferedWriter& operator<<(const string& str);
	inline bufferedWriter& operator<<(const char* str) { return bufferedWriter::operator<<(string(str)); }
	
	inline bufferedWriter& flush() 
	{ 
		writeThread();
		
		if(_writeThread.joinable())
			_writeThread.join();
		
		return *this;
	}
};

//TODO: th. writing

#endif //_BUFFERED_WRITER_H_
