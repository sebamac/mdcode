#ifndef __LOGGER_H_
#define __LOGGER_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>

using namespace std;
using namespace std::chrono;

#define LOGGER Logger::get()

class Logger
{
private:
	string _filePath;
	ofstream _logger;
	ostringstream _buffer;

	string currentTime();
	
	Logger() noexcept;
	
public:
	Logger(const Logger&  ) = delete;
	Logger(const Logger&& ) = delete;
	Logger& operator=(const Logger& ) = delete;
	Logger& operator=(Logger&& ) = delete;
	~Logger() = default;
	static inline Logger& get()
	{	
		static Logger log;
		return log;
	}

	Logger& open(const char* path, ios_base::openmode mode = ios_base::app);
	inline Logger& open(const string& path, ios_base::openmode mode = ios_base::app) { return open(path.c_str(), mode); }
	Logger& clear();
	Logger& close();
	Logger& flush();
	inline Logger& endl() { _buffer<<"\n"; return flush(); }
	
	template <class T>
	inline Logger& push_back(const T& value)
	{
		_buffer<<value;
		return *this;
	}
	
	template <class T>
	inline Logger& operator<<(const T& value)
	{
		_buffer<<value;
		return *this;
	}
	
	//puntatore a funzione
	inline Logger& operator<<(Logger& (*pt)(Logger& )) { return pt(*this); }
};

extern inline Logger& endl(Logger& log)  { return log.endl(); }
extern inline Logger& flush(Logger& log) { return log.flush(); }

#endif //__LOGGER_H_
