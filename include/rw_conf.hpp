#ifndef __RW_CONF_H_
#define __RW_CONF_H_

#include <iostream>
#include <fstream>
#include <iomanip>
#include <limits>
#include <vector>
#include <algorithm>
#include <system_error>
#include <exception>

#include "coordinates.hpp"
#include "bufferedWriter.hpp"
#include "xyzFrame.hpp"

using namespace std;

class energyWriter: public bufferedWriter
{
private:
	char _buffer[48];
	
public:
	energyWriter() = delete;
	energyWriter(const string& s, const bool& append = false, const size_t &lines = 50);
	energyWriter(energyWriter&  ) = delete;
	energyWriter(energyWriter&& ) = delete;
	energyWriter& operator=(energyWriter&  ) = delete;
	energyWriter& operator=(energyWriter&& ) = delete;
	virtual ~energyWriter() = default;
	
	energyWriter& write(const float& time, const double& en, const float& temp);
};

#ifndef kFrame
#define kFrame 8
#endif
class trajWriter: public bufferedWriter
{
private:
	char _buffer[65];
public:
	trajWriter() = delete;
	trajWriter(const size_t& nat, const string& f, const bool& append = false);
	trajWriter(trajWriter&  ) = delete;
	trajWriter(trajWriter&& ) = delete;
	trajWriter& operator=(trajWriter&  ) = delete;
	trajWriter& operator=(trajWriter&& ) = delete;
	virtual ~trajWriter() = default;

	trajWriter& writeFrame(const xyzFrame& fr, const char* comment);
	inline trajWriter& writeFrame(const xyzFrame& fr) { return writeFrame(fr, fr.commentLine().c_str()); }
	inline trajWriter& writeFrame(const xyzFrame& fr, const string& comment) { return writeFrame(fr, comment.c_str()); }
	
	void inline operator<<(const xyzFrame& fr) { writeFrame(fr, fr.commentLine().c_str()); };
};

class trajVWriter: public bufferedWriter
{
private:
	char _buffer[80];
public:
	trajVWriter() = delete;
	trajVWriter(const size_t& nat, const string& f, const bool& append = false);
	trajVWriter(trajVWriter&  ) = delete;
	trajVWriter(trajVWriter&& ) = delete;
	trajVWriter& operator=(trajVWriter&  ) = delete;
	trajVWriter& operator=(trajVWriter&& ) = delete;
	virtual ~trajVWriter() = default;

	trajVWriter& writeFrame(const vxyzFrame& fr, const char* comment);
	inline trajVWriter& writeFrame(const vxyzFrame& fr) { return writeFrame(fr, fr.commentLine().c_str()); }
	inline trajVWriter& writeFrame(const vxyzFrame& fr, const string& comment) { return writeFrame(fr, comment.c_str()); }
	
	inline void operator<<(const vxyzFrame& fr) { writeFrame(fr, fr.commentLine().c_str()); };
};


/*
HEADER: N° di atomi,Precision,TimeStep,List,END
		16bit		8bit	16bit	Array di 8bit
		uint16_t uint8_t uint16_t uint8_8 uint8_t ... END
FR1:	(uint8_t uint32_t uint32_t uint32_t)_1 ... (uint8_t uint32_t uint32_t uint32_t)_N END
FR2: 	...
FRN:    (uint8_t uint32_t uint32_t uint32_t)_1 ... (uint8_t uint32_t uint32_t uint32_t)_N END
		END

Precision: 1 -> x10
		   2 -> x100
		   i -> x10*i
*/

// class trajCompWriter: public bufferedWriter
// {
// 	typedef uint8_t unsigned char;
// 	typedef uint32_t unsigned int;
// private:
// 	uint8_t *buf; //[fr.size()*13+2]
// 	const uint16_t _timeStep;
// 	const uint8_t _precision;

// public:
// 	trajCompWriter() = delete;
// 	trajCompWriter(const string& name, const xyzFrame& fr, const size_t& precision, const size_t& timeStep);
// 	trajCompWriter(const trajCompWriter& ) = delete;
// 	trajCompWriter(trajCompWriter&& trj);
	
// 	trajCompWriter& operator=(const trajCompWriter& ) = delete;
// 	trajCompWriter& operator=(trajCompWriter&& trj);
	
// 	void writeFrame(const xyzFrame& fr);
	
// 	trajCompWriter& operator<<(const xyzFrame& fr){ writeFrame(fr); return *this; }
// };

template <class frame_type>
class trajReader
{
protected:
	string _fileName;
	float _dt;
	size_t _frameIndex, _frameSize, _frames;

	vector<string> _species;

	ifstream _trajStream;

public:
	trajReader() noexcept: _fileName(""), _dt(0), _frameIndex(0) {}
	trajReader(const string& name): _fileName(name), _dt(0), _frameIndex(0)
	{
		open(_fileName, 0);
	}
	trajReader(const string& name, const float& dt): _fileName(name), _dt(dt), _frameIndex(0)
	{
		open(_fileName, _dt);
	}
	trajReader(const trajReader&  ) = delete;
	trajReader(trajReader&& obj): _fileName(std::move(obj._fileName)), _dt(0), _frameIndex(0), _frameSize(0), _frames(0),
								  _species(std::move(obj._species)), _trajStream(std::move(obj._trajStream))				 
	{
		std::swap(_dt, obj._dt);
		std::swap(_frameIndex, obj._frameIndex);
		std::swap(_frameSize, obj._frameSize);
		std::swap(_frames, obj._frames);
	}
		
	~trajReader()
	{
		close();
	}
	void open(const string& name, const float& dt)
	{
		_dt = dt;
		_fileName = name;
	
		_trajStream.open(_fileName.c_str());
		_frameIndex = 0;
	
		frame_type temp;
		_trajStream>>temp;
		_frameSize = _trajStream.tellg();
		
		_species.push_back(temp.atomLabel(0));
		for(size_t i = 1; i < temp.size(); ++i)
		{
			if(_species.back() != temp.atomLabel(i)) 
				_species.push_back(temp.atomLabel(i));
		}
	
		_trajStream.seekg(0, ios_base::end);
		size_t totalSize = _trajStream.tellg();
		_frames = totalSize/_frameSize;
		_trajStream.seekg(0, ios_base::beg);
	}
	void close()
	{ 
		_trajStream.close(); 
	}
	void rewind() 
	{ 
		_trajStream.seekg(0, ios_base::beg); 
	}
	void skipFrame(const size_t& n = 1) 
	{
		_trajStream.seekg((long)_trajStream.tellg() + (_frameSize*n)); 
	}
	const float time() const noexcept
	{ 
		return _frameIndex*_dt; 
	}
	const size_t& nFrames() const noexcept
	{ 
		return _frames; 
	}
	const size_t& frameIndex() const noexcept
	{ 
		return _frameIndex; 
	}
	const size_t numberOfSpecies() const noexcept
	{ 
		return _species.size(); 
	}
	const string& species(const size_t& i) const 
	{ 
		return _species.at(i); 
	}

	operator bool() const noexcept
	{ 
		return (bool)_trajStream; 
	}
	trajReader& operator>>(frame_type& frame) 
	{ 
		return getFrame(frame); 
	}
	trajReader& getFrame(frame_type& frame)
	{
		if(_frameIndex == _frames)
		{
			_trajStream.setstate(ios_base::eofbit | ios_base::failbit  | ios_base::badbit | ~(ios_base::goodbit));
			return *this;
		}
	
		_trajStream>>frame;
		_frameIndex++;
		
		return *this;
	}
	
};

#endif //__RW_CONF_H_
