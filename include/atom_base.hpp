#ifndef __ATOM_BASE_H_
#define __ATOM_BASE_H_

#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <limits>
#include <algorithm>
#include <system_error>
#include <exception>

#include "xyzFrame.hpp"

using namespace std;

class atomTypesList;

class atomType
{
	friend class atomTypesList;
	
private:
	string _label;
	float _mass, _LJsigma, _LJepsilon, _LJsigma6;
	double _inverseMass, _sqrtMass;
	
public:
	atomType() noexcept;
	atomType(const string label, const float mass, 
			  const float sigma, const float epsilon) noexcept;
	atomType(const atomType &a) noexcept;
	~atomType() = default;
	atomType& operator=(const atomType &a) noexcept;														
	
	bool operator< (const atomType& val) const noexcept { return _label <  val._label; }
	bool operator==(const atomType& val) const noexcept { return _label == val._label;}
	bool operator==(const string& val)   const noexcept { return _label == val;}

	const inline string& label()    	const noexcept { return _label; }
	const inline float&  mass()     	const noexcept { return _mass; }
	const inline double& inverseMass()  const noexcept { return _inverseMass; }
	const inline double& sqrtMass() 	const noexcept { return _sqrtMass; }
	const inline float&  sigma()    	const noexcept { return _LJsigma; }
	const inline float&  sigma6()   	const noexcept { return _LJsigma6; }
	const inline float&  epsilon()  	const noexcept { return _LJepsilon; }

	friend istream& operator>>(istream &str, atomType& val)
	{
		str>>val._label
	   	   >>val._LJsigma
	       >>val._LJepsilon
	       >>val._mass;
		
		val._LJsigma6 = pow(val._LJsigma, 6.);
		val._sqrtMass = sqrt((double)val._mass);
		val._inverseMass = (double)1.0/val._mass;

		return str;
	}

	friend ostream& operator<<(ostream &str, const atomType& val)
	{
		str<<val._label<<"\t"
	   	   <<val._LJsigma<<"\t"
	       <<val._LJepsilon<<"\t"
	       <<val._mass<<"\t";

		return str;
	}
};

class atomTypesList
{
private:
	vector< vector<float> > _epsilon;
	vector< vector<float> > _sigma;
	vector< vector<float> > _sigma6;
	
protected:
	size_t *_atomTypesIndex;
	vector< atomType > _atomTypesVec;
	
public:
	atomTypesList();
	atomTypesList(const string fileName);
	atomTypesList(const string fileName, const xyzFrame &fr);
	virtual ~atomTypesList();

	bool linkFrame(const xyzFrame &fr);
	bool linkFrame(const string conf);
	void open(const string type);

	const atomType& operator[](const string str) const;
	const atomType& at(const string str) const;
	const inline atomType& operator[](const size_t i) const { return _atomTypesVec[_atomTypesIndex[i]]; }
	const inline atomType& at(const size_t i) const { return _atomTypesVec.at(_atomTypesIndex[i]); }
	
	const inline float& epsilon(const size_t i, const size_t j) const { return _epsilon[_atomTypesIndex[i]][_atomTypesIndex[j]]; }
	const inline float& sigma  (const size_t i, const size_t j) const { return _sigma[_atomTypesIndex[i]][_atomTypesIndex[j]]; }
	const inline float& sigma6 (const size_t i, const size_t j) const { return _sigma6[_atomTypesIndex[i]][_atomTypesIndex[j]]; }
	
	inline pair<float, float> LJ_matrix(const size_t i, const size_t j) const { return make_pair(_epsilon[i][j], _sigma[i][j]); }
	const inline atomType& atom(const size_t i) const { return _atomTypesVec.at(i); }
	const inline size_t nspecies() const { return _atomTypesVec.size(); }
};

#endif //__ATOM_BASE_H_
