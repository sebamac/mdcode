#ifndef __INTEGRATOR_H_
#define __INTEGRATOR_H_

#include "coordinates.hpp"
#include "atoms.hpp"
#include "constants.h"
#include "settings.hpp"

class integrator
{
protected:
	const double timeStep;
	Atoms *atoms;
public:
	integrator() = delete;
	integrator(Atoms *a, Settings *s) noexcept;
	integrator(integrator&  ) = delete;
	integrator(integrator&& ) = delete;
	integrator& operator=(integrator&  ) = delete;
	integrator& operator=(integrator&& ) = delete;
	virtual ~integrator() = default;

	virtual void nextCoordinates() noexcept = 0;
	virtual void nextVelocities() noexcept = 0;
	
	virtual void integrate() noexcept = 0;
};

class thermostat
{
protected:
	float _temp;

public:
	thermostat() = delete;
	thermostat(const float& temp) noexcept;
	thermostat(thermostat&  ) = delete;
	thermostat(thermostat&& ) = delete;
	thermostat& operator=(thermostat&  ) = delete;
	thermostat& operator=(thermostat&& ) = delete;
	virtual ~thermostat() = 0;

	virtual const float& temperature() const noexcept { return _temp; }
	virtual void setTemperature(const float& temp) noexcept { _temp = temp; }
};

class velocityVerlet: public integrator
{
protected:
	const double dStep;
	const double dStep2;
	tvec prevForces;

public:
	velocityVerlet() = delete;
	velocityVerlet(Atoms *a, Settings *s);
	velocityVerlet(velocityVerlet&  ) = delete;
	velocityVerlet(velocityVerlet&& ) = delete;
	velocityVerlet& operator=(velocityVerlet&  ) = delete;
	velocityVerlet& operator=(velocityVerlet&& ) = delete;
	virtual ~velocityVerlet() = default;

	virtual void nextVelocities() noexcept;
	virtual void nextCoordinates() noexcept;

	virtual inline void integrate() noexcept
	{
		nextCoordinates();
		atoms->evalForces();
		nextVelocities();
	}

};

class velocityVerletAnderson: public velocityVerlet, public thermostat
{
private:
	const double _eta;
	double _scaling;

	std::mt19937_64 _generator;
	std::normal_distribution<double> _gausGen;
	std::uniform_real_distribution<double> _uniformGen;

public:
	velocityVerletAnderson() = delete;
	velocityVerletAnderson(Atoms *a, Settings *s);
	velocityVerletAnderson(velocityVerletAnderson&  ) = delete;
	velocityVerletAnderson(velocityVerletAnderson&& ) = delete;
	velocityVerletAnderson& operator=(velocityVerletAnderson&  ) = delete;
	velocityVerletAnderson& operator=(velocityVerletAnderson&& ) = delete;
	virtual ~velocityVerletAnderson() = default;

	virtual void nextVelocities() noexcept;

	virtual void setTemperature(const float& temp) noexcept;
};

class velocityVerletBussi: public velocityVerlet, public thermostat
{
private:
	const double _dtOver2Tau;
	double _K0; //target kinetic energy

	std::mt19937_64 _generator;
	std::normal_distribution<double> _gausGen;
	std::gamma_distribution<double> _gammaGen;

	void rescaleVelocity() noexcept;

public:
	velocityVerletBussi() = delete;
	velocityVerletBussi(Atoms *a, Settings *s);
	velocityVerletBussi(velocityVerletBussi&  ) = delete;
	velocityVerletBussi(velocityVerletBussi&& ) = delete;
	velocityVerletBussi& operator=(velocityVerletBussi&  ) = delete;
	velocityVerletBussi& operator=(velocityVerletBussi&& ) = delete;
	virtual ~velocityVerletBussi() = default;

	virtual inline void integrate() noexcept
	{
		velocityVerlet::nextCoordinates();
		atoms->evalForces();
		velocityVerlet::nextVelocities();
		atoms->calcKineticEnergy();
		velocityVerletBussi::rescaleVelocity();
	}

	virtual void setTemperature(const float& temp) noexcept;
};

#endif // __INTEGRATOR_H_
