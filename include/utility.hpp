#ifndef _UTILITY_H_
#define _UTILITY_H_

#include <boost/numeric/ublas/matrix.hpp> 
#include <boost/numeric/ublas/lu.hpp> 

template<class T> 
boost::numeric::ublas::matrix<T> invertMatrix(const boost::numeric::ublas::matrix<T> &input) 
{ 
    using namespace boost::numeric::ublas;
    matrix<T> inverse = zero_matrix<T>(input.size1(), input.size2());

    if(input.size1() != input.size2()) return inverse;

    // create a working copy of the input 
    matrix<T> A(input);

    // create a permutation matrix for the LU-factorization 
    permutation_matrix<std::size_t> pm(A.size1()); 

    // perform LU-factorization 
    int res = lu_factorize(A, pm); 
    if( res != 0 )  return inverse; 

    // create identity matrix of "inverse" 
    inverse.assign(identity_matrix<T>(A.size1()));

    // backsubstitute to get the inverse 
    lu_substitute(A, pm, inverse);

    return inverse; 
}

// template<class T> 
// boost::numeric::ublas::matrix<T> invertMatrix(const boost::numeric::ublas::matrix<T> &input) 
// {
//     using namespace boost::numeric::ublas;
	
// 	if(input.size1() != 3 || input.size2() != 3) return zero_matrix<T>(3,3);
	
// 	matrix<T> inverse = zero_matrix<T>(3,3);
	
// 	inverse(0,0) = input(1,1)*input(2,2) - input(1,2)*input(2,1);
// 	inverse(1,0) = input(1,2)*input(2,0) - input(1,0)*input(2,2);
// 	inverse(2,0) = input(1,0)*input(2,1) - input(1,1)*input(2,0);
// 	inverse(0,1) = input(0,2)*input(2,1) - input(0,1)*input(2,2);
// 	inverse(1,1) = input(0,0)*input(2,2) - input(0,2)*input(2,0);
// 	inverse(2,1) = input(0,1)*input(2,0) - input(0,0)*input(2,1);
// 	inverse(0,2) = input(0,1)*input(1,2) - input(0,2)*input(1,1);
// 	inverse(1,2) = input(0,2)*input(1,0) - input(0,0)*input(1,2);
// 	inverse(2,2) = input(0,0)*input(1,1) - input(0,1)*input(1,0);
	
// 	double det = input(0,0)*inverse(0,0) + input(0,1)*inverse(1,0) + input(0,2)*inverse(2,0);
	
// 	return inverse/det;
// }

#endif
