#ifndef __SETTINGS_H_
#define __SETTINGS_H_

#include <iostream>
#include <fstream>
#include <string>
#include <chrono>

#include <boost/program_options.hpp>

using namespace std;
using namespace boost;
using namespace program_options;

class Settings
{
private:
	Settings();
	static Settings *settings_instance;
	
	float _time_step;
	size_t _nsteps;
	
	size_t _nstxfinal, _nstcomm, _nstcalcenergy;
	size_t _nstxout, _nstvout, _nstfout, _nstlog, _nstenergy, _nstout_comp;

	string _tcoupl, _pcoupl;
	float _ref_t, _ref_t_final, _t_rate, _t_increment, _anderson_eta, _bussi_tau;
	size_t _t_seed;
	float _ref_p;

	bool _gen_vel;
	float _gen_temp;
	size_t _gen_seed;

public:
	Settings(Settings&) = delete;
	void operator=(Settings const &) = delete;
	static Settings* get()
	{	
		return (settings_instance) ? settings_instance : settings_instance = new Settings;
	}
	~Settings();
	
	bool import(const string& file_name);
	
	const inline float& dt() const   	{ return _time_step; }              
	const inline size_t& nsteps() const { return _nsteps; }       
	  
	const inline size_t& nstcomm() const 	   { return _nstcomm; }
	const inline size_t& nstxfinal() const	   { return _nstxfinal; }
	const inline size_t& nstxout() const 	   { return _nstxout; }         
	const inline size_t& nstvout() const 	   { return _nstvout; }
	const inline size_t& nstfout() const 	   { return _nstfout; }
	const inline size_t& nstlog() const 	   { return _nstlog; }
	const inline size_t& nstenergy() const 	   { return _nstenergy; }
	const inline size_t& nstout_comp() const   { return _nstout_comp; }
	const inline size_t& nstcalcenergy() const { return _nstcalcenergy; }
	
	const inline string& tcoupl() const 	  { return _tcoupl; }
	const inline float&  ref_t() const 	   	  { return _ref_t; }
	const inline float&  ref_t_final() const  { return _ref_t_final; }
	const inline float&  t_rate() const 	  { return _t_rate; }
	const inline float&  t_increment() const  { return _t_increment; }
	const inline float&  anderson_eta() const { return _anderson_eta; }
	const inline float&  bussi_tau() const    { return _bussi_tau; }
	const inline size_t& t_seed() const       { return _t_seed; }

	const inline string& pcoupl() const { return _pcoupl; }
	const inline float&  ref_p() const  { return _ref_p; }
	
	const inline bool&   gen_vel() const  { return _gen_vel; }
	const inline float&  gen_temp() const { return _gen_temp; }
    const inline size_t& gen_seed() const { return _gen_seed; }

    friend ostream& operator<<(ostream& str, const Settings& set)
    {
    	str<<"dt            = "<<set.dt()<<endl;
		str<<"nsteps        = "<<set.nsteps()<<endl;
		str<<"nstcomm       = "<<set.nstcomm()<<endl; 	 
		str<<"nstxfinal     = "<<set.nstxfinal()<<endl;  
		str<<"nstxout       = "<<set.nstxout()<<endl; 
		str<<"nstvout       = "<<set.nstvout()<<endl;   
		str<<"nstfout       = "<<set.nstfout()<<endl;     
		str<<"nstlog        = "<<set.nstlog()<<endl;
		str<<"nstenergy     = "<<set.nstenergy()<<endl;
		str<<"nstout_comp   = "<<set.nstout_comp()<<endl;
		str<<"nstcalcenergy = "<<set.nstcalcenergy()<<endl<<endl;
		
		str<<"tcoupl        = "<<set.tcoupl()<<endl;
		if(set.tcoupl() != "no")
		{
			str<<"ref_t         = "<<set.ref_t()<<endl;
			str<<"t_seed        = "<<set.t_seed()<<endl;
		}
		if(set.tcoupl() == "anderson")
			str<<"anderson_eta  = "<<set.anderson_eta()<<endl;
		if(set.tcoupl() == "bussi")
			str<<"bussi_tau     = "<<set.bussi_tau()<<endl;
		if(set.ref_t_final() > 0)
		{
			str<<"ref_t_final   = "<<set.ref_t_final()<<endl;
			str<<"t_increment   = "<<set.t_increment()<<endl;
			str<<"t_rate        = "<<set.t_rate()<<endl;
		}
		
		str<<"pcoupl        = "<<set.pcoupl()<<endl;
		if(set.pcoupl() != "no")
			str<<"ref_p         = "<<set.ref_p()<<endl<<endl;
		
		str<<"gen_vel       = "<<(set.gen_vel() ? "yes" : "no")<<endl;
		if(set.gen_vel())
		{
			str<<"gen_temp      = "<<set.gen_temp()<<endl;
			str<<"gen_seed      = "<<set.gen_seed()<<endl;
		}

		return str;
    }
}; 

#endif
