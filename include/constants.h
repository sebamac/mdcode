#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

#include <math.h>

extern const double kBoltz_eV2K; // eV/K
extern const double kBoltz_K2eV;
extern const double amu;
extern const double eVtoJ;
extern const double Jtoev;
extern const double amu2eV;
extern const double inverse_amu2eV;

#endif //_CONSTANTS_H_
