#ifndef _AVERAGER_H_
#define _AVERAGER_H_

#include <boost/circular_buffer.hpp>

using namespace std;
using namespace boost;

template <class T>
class averager
{
private:
	T _average, _sum;
	size_t _size, _index;

public:
	averager() = delete;
	averager(const size_t& n): _average(0), _sum(0), _size(n), _index(0) 
	{
		static_assert(std::is_arithmetic<T>::value, "averager work only with arithmetic type!");
	}
	averager(const averager& av): _average(av._average), _sum(av._sum), _size(av._size), _index(av._index)
	{
		static_assert(std::is_arithmetic<T>::value, "averager work only with arithmetic type!");
	}
	averager(averager&& av) noexcept: _average(0), _sum(0), _size(0), _index(0)
	{
		std::swap(_average, av._average);
		std::swap(_sum, av._sum);
		std::swap(_index, av._index);
		std::swap(_size, av._size);
	}
	~averager() = default;
	
	averager& operator=(const averager& av) noexcept
	{
		_average = av._average;
		_sum = av._sum;
		_size = av._size;
		_index = av._index;

		return *this;
	}
	averager& operator=(averager&& av) noexcept
	{
		std::swap(_average, av._average);
		std::swap(_sum, av._sum);
		std::swap(_index, av._index);
		std::swap(_size, av._size);

		return *this;
	}

	template<class U>
	void push_back(const U& val) noexcept
	{
		_sum+= val;
		_index++;

		if(_index == _size)
		{
			_average = _sum / _size;
			_sum = _index = 0;
		}
	}
	template<class U>
	inline averager& operator+=(const U& val) noexcept
	{
		push_back(val);
		return *this;
	}
	
	template<class U>
	inline averager& operator+ (const U& val) noexcept
	{
		push_back(val);
		return *this;
	}

	inline void clear() noexcept
	{
		_average = 0;
		_sum = 0;
		_index = 0;
	}

	inline const T& average() const noexcept { return _average; }
	inline const size_t& size() const noexcept { return _size; }

};


template <class T>
class circularAverager: public circular_buffer<T>
{
private:
	typedef circular_buffer<T> container_type;

public:
	//static_assert(std::is_arithmetic<T>::value, "circularAverager work only with arithmetic type!");
	
	circularAverager(): container_type() {}
	circularAverager(const size_t n): container_type(n) {}
	circularAverager(const circularAverager& av): container_type(av) {}
	circularAverager(circularAverager&& av) noexcept: container_type(std::move(av)) {}
	~circularAverager() { container_type::clear(); }
	
	circularAverager& operator<<(const T& val) 
	{ 
		push_back(val); 
		return *this; 
	}
	circularAverager& operator<<(T&& val) 
	{ 
		push_back(std::move(val)); 
		return *this; 
	}
	
	
	T average()
	{
		return sum()/container_type::size();
	}
	T sum()
	{
		return std::accumulate(container_type::begin(), container_type::end(), (T)0.);
	}
	T stdDev()
	{
		return average_deviation().second;
	}
	
	pair<T,T> average_deviation()
	{
		pair<T,T> meanDev = std::accumulate(container_type::begin(), container_type::end(), make_pair(0.,0.), 
			[](const pair<T,T>& sum, const T& val)->pair<T,T>
			  {
				 return make_pair(sum.first + val, sum.second + val*val);
			  });
		meanDev.first /= container_type::size();
		meanDev.second/= container_type::size(); 
		meanDev.second = sqrt(meanDev.second - meanDev.first*meanDev.first);
		
		return meanDev;
	}
};

#endif //  _AVERAGER_H_
