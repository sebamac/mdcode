#ifndef __ATOMS_H_
#define __ATOMS_H_

#include <array>
#include <random>

#include "coordinates.hpp"
#include "atom_base.hpp"
#include "rw_conf.hpp"
#include "constants.h"
#include "bufferedWriter.hpp"

using namespace std;

class Atoms: public vxyzFrame, public atomTypesList
{
	friend class atomType;
	friend class velocityVerlet;
	friend class velocityVerletAnderson;
	friend class velocityVerletBussi;

private:
	float mass, _inverseMass, _temperature;
	double potEnergy, kinEnergy;
	size_t _DOF;
	tvec fvec;

public:
	Atoms() = default;
	Atoms(const string conf, const string type);
	Atoms(Atoms& ) = delete;
	Atoms(Atoms&&) = delete;
	Atoms& operator=(const Atoms& ) = delete;
	Atoms& operator=(const Atoms&&) = delete;
	virtual ~Atoms() = default;
	
	void open(const string str) = delete;
	void open(const string fr, const string type);
	void write(const string fileName, const float time);
		
	void initVelocities(const float temp, const unsigned int seed);
	void evalForces(); //compute forces and potential energy
	void commRemoval();
	void rotmRemoval(); //expensive routine!
	void calcKineticEnergy();
	double fMax();

	void centerOfMass(coordinate& );
	void linearMomentum(coordinate& );
	void angularMomentum(coordinate& );
	array< array<double, 3>, 3> inertiaMatrix();
	array< array<double, 3>, 3> invertedInertiaMatrix();

	const inline float&  temperature() const { return _temperature; }
	const inline double& kineticEnergy() const { return kinEnergy; } 	//eV
	const inline double& potentialEnergy() const { return potEnergy; }	//eV
	const inline float&  systamMass() const { return mass; }			//uma
	const inline tvec&   forces() const { return fvec; }

	template <class traj_type>
	friend void inline operator<<(traj_type &trj, const Atoms &atm)
	{
		char buf[33];
	 	sprintf(buf, "%13.8f %13.8f", atm.potEnergy, atm.potEnergy + atm.kinEnergy);
		trj.writeFrame(atm, buf);
	}	
};

#endif // __ATOMS_H_
