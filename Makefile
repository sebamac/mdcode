
PROG=md_run
#GCC=clang++-mp-4.0 -std=c++14
#FLAGS=-Ofast -funroll-all-loops -march=core2 -Wall -Wpedantic -pthread -fopenmp
#LIBS=-lboost_program_options-mt -lboost_system-mt -lboost_filesystem-mt
GCC=g++-7
FLAGS=-O3 -funroll-all-loops -march=core-avx2 -Wall -Wpedantic -pthread -fopenmp
LIBS=-lboost_program_options -lboost_system -lboost_filesystem

SOURCES_DIR=./src
INCLUDE_DIR=-I./include
#LIB_DIR=
#INCLUDE_DIR=-I./include -I/opt/local/include
#LIB_DIR=-L/opt/local/lib
O_DIR=./obj

SOURCES:=$(shell find $(SOURCES_DIR) -name '*.cpp')

OBJ:=$(addprefix $(O_DIR)/,$(addsuffix .o,$(notdir $(SOURCES))))

all: $(PROG)

$(PROG):  $(OBJ)
	$(GCC) -o $(PROG) $(OBJ) $(INCLUDE_DIR) $(LIB_DIR) $(LIBS) $(FLAGS)

$(O_DIR)/%.cpp.o: $(SOURCES_DIR)/%.cpp
	$(GCC) -c $(INCLUDE_DIR) $(FLAGS) -o $@ $<


.PHONY: clean
clean:
	rm -f $(O_DIR)/*.o
	rm -f ~* $(O_DIR)/~*
	rm -f $(SOURCES_DIR)/~*
	rm -f $(PROG)