#include <iostream>
#include <boost/program_options.hpp>

#include "xyzFrame.hpp"
#include "rw_conf.hpp"

using namespace std;
using namespace boost;
using namespace program_options;

int main(int ac, char *av[])
{
	string inputFileName, outFileName;
	//size_t dumpFrame;

	options_description optDesc("Allowed options");
	optDesc.add_options()("help,h", "print help messages;");
	optDesc.add_options()("in-file,f", value< string >(&inputFileName)->required(), "input file;");
	optDesc.add_options()("out-file,o", value< string >(&outFileName)->default_value("out.dat"), "output file name;");

	variables_map options;
    store(parse_command_line(ac, av, optDesc), options);
	if(options.count("help"))
	{
		cout<<optDesc<<endl;
		exit(0);
	}
	notify(options);

	trajReader<vxyzFrame> inTraj(inputFileName);
	cout<<"Total number of frames: "<<inTraj.nFrames()<<endl;
	vector<double> coorVel;
	coorVel.reserve(inTraj.nFrames());

	vxyzFrame frame;
	tvec initVel;
	while(inTraj>>frame)
	{
		if((inTraj.frameIndex() % 100) == 0)
			cout<<"\rFrame: "<<inTraj.frameIndex()<<flush;
		
		if(inTraj.frameIndex() == 1)
			initVel = frame.velocities();

		tvec vel(frame.velocities());
		double mean = 0;
		for(size_t i = 0; i < vel.size(); ++i)
		{
			mean += initVel[i][0]*vel[i][0] + initVel[i][1]*vel[i][1] + initVel[i][2]*vel[i][2];
		}
		coorVel.push_back(mean/vel.size());
	}

	cout<<endl<<"Write output..."<<endl;

	ofstream file(outFileName.c_str());

	if(file)
	{
		for(size_t i = 0; i < coorVel.size(); ++i)
		{
			file<<i<<" "<<coorVel[i]/coorVel[0]<<endl;
		}
		file.close();
	}
}
