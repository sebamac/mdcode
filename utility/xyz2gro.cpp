#include <iostream>
#include <boost/program_options.hpp>

#include "/home/sebastian/script/lib/readgro.h"
#include "xyzFrame.hpp"

using namespace std;
using namespace boost;
using namespace program_options;

int main(const int ac, const char *av[])
{
	string outFileName, inputFileNames;
	string xyzUnit;
	vector<float> groBox(3,0);

	positional_options_description posOpt;
	posOpt.add("in-file", 0);

	options_description optDesc("Allowed options");
	optDesc.add_options()("help,h", "print help messages;");
	optDesc.add_options()("in-file,f", value< string >(&inputFileNames)->required(), "input .xyz file;");
	optDesc.add_options()("out-file,o", value< string >(&outFileName)->default_value("out.gro"), "output .gro file;");
	optDesc.add_options()("unit,u", value< string >(&xyzUnit)->default_value("A"), "Unit for the .xyz file; A, nm.");
	//optDesc.add_options()("box", value< vector<float> >(&groBox)->default_value(vector<float>(3,0)));

	variables_map options;
	store(command_line_parser(ac, av)
			.options(optDesc)
			.positional(posOpt)
			.run(), 
		 options);
	if(options.count("help"))
	{
		cout<<optDesc<<endl;
		exit(0);
	}
	notify(options);

	float unit = 1;
	if(xyzUnit == "A") unit = 0.1;

	xyzFrame xyz(inputFileNames);
	groAtomList gro;
	gro.resize(xyz.size());

	for(size_t i = 0; i < xyz.size(); ++i)
	{
		groAtom atm;
		atm.resName()    = xyz.atomLabel(i);
		atm.atomName()   = xyz.atomLabel(i);
		atm.resNumber()  = i + 1;
		atm.atomNumber() = i + 1;
		atm.x() = xyz.x(i) * unit;
		atm.y() = xyz.y(i) * unit;
		atm.z() = xyz.z(i) * unit;

		gro.at(i) = atm;
	}

	gro.box(groBox[0], groBox[1], groBox[2]);
	gro.renumber();
	gro.printgroFile(outFileName);
}