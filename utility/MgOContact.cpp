#include <iostream>
#include <boost/program_options.hpp>

#include "xyzFrame.hpp"
#include "rw_conf.hpp"

using namespace std;
using namespace boost;
using namespace program_options;

int main(int ac, char *av[])
{
	string inputFileName, atom = "";
	vector<float> box;
	float cutOff;
	size_t ave;

	options_description optDesc("Allowed options");
	optDesc.add_options()("help,h", "print help messages;");
	optDesc.add_options()("in-file,f", value< string >(&inputFileName)->required(), "input file in .xyz format;");
	optDesc.add_options()("cut-off,d", value< float >(&cutOff)->required(), "distance cut-off [A];");
	optDesc.add_options()("box,b", value< vector<float> >(&box)->required()->multitoken(), "box dimensions (space separated);");
	optDesc.add_options()("atom", value< string >(&atom), "atom label to calculate contact between interface");
	optDesc.add_options()("ave", value< size_t >(&ave)->default_value(1), "average frame;");

	//supose the center of the box at geometric center (-10,10)(-10,10)(-10,10)

	variables_map options;
    store(parse_command_line(ac, av, optDesc), options);
	if(options.count("help"))
	{
		cout<<optDesc<<endl;
		exit(0);
	}
	notify(options);
	
	if(inputFileName.substr(inputFileName.rfind('.')+1) != "xyz")
	{
		cerr<<"Input file is not a .xyz format!"<<endl<<endl;
		exit(1);
	}
		
	trajReader<xyzFrame> inTraj(inputFileName);
	cout<<"Total number of frames: "<<inTraj.nFrames()<<endl;
	
	if(atom.empty())
	{
		while(1)
		{
			cout<<"Species found: "<<endl;
			for(size_t i = 0; i < inTraj.numberOfSpecies(); ++i)
				cout<<"\t "<<i<<": "<<inTraj.species(i)<<endl;
			cout<<"Select one atom: "<<endl;
			
			int a1=-1;
			cin>>a1;
			
			if(a1 < 0 || a1 > inTraj.numberOfSpecies())
				cout<<"The input selection must be between 0 and "<<inTraj.numberOfSpecies()<<"!\n"<<endl;
			else
			{
				cout<<endl;
				atom = inTraj.species(a1);		
				break;
			}
		}
	}
	
	vector<size_t> contact;
	contact.reserve(inTraj.nFrames());

	xyzFrame frame;
	size_t count = 0, index = 0;
	while(inTraj>>frame)
	{
		if((inTraj.frameIndex() % 1000) == 0)
			cout<<"\rFrame: "<<inTraj.frameIndex()<<flush;

		for(size_t i = 0; i < frame.size(); ++i)
		{
			if(frame.atomLabel(i) != atom) continue;
			
			for(size_t j = 0; j < 3; ++j)
			{
				if( (box[j]/2. - fabs(frame[i][j])) <= cutOff )
					count++;
			}
		}
		index++;
		if(index == ave)
		{
			contact.push_back(count/ave);
			index = 0;
			count = 0;
		}
	}
	inTraj.close();

	ostringstream temp;
	temp<<"contact_"<<atom<<".dat";
	ofstream outFile(temp.str().c_str());
	
	temp.str("");
	temp.clear();
	for(size_t i = 0; i < ac; ++i)
		temp<<av[i]<<" ";
	
	outFile<<"#"<<temp.str()<<endl;
	outFile<<"#Contact between "<<atom<<" and box walls; within "<<cutOff/10.<<" nm"<<endl;
	for(size_t j = 0; j < contact.size(); ++j)
	{
		outFile<<scientific<<setw(6)<<setprecision(0)<<(j+1)*ave<<" ";
		outFile<<fixed<<contact[j]<<endl;
	}
	outFile.close();

	cout<<endl<<"Done"<<endl;
}
