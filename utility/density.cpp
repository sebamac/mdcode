#include <iostream>
#include <boost/program_options.hpp>

#include "xyzFrame.hpp"
#include "rw_conf.hpp"

using namespace std;
using namespace boost;
using namespace program_options;

int main(int ac, char *av[])
{
	char dir;
	size_t sampling, nSlabs;
	vector<float> box;
	string inputFileName, atom;

	options_description optDesc("Allowed options");
	optDesc.add_options()("help,h", "print help messages;");
	optDesc.add_options()("in-file,f", value< string >(&inputFileName)->required(), "input files;");
	optDesc.add_options()("direction,d", value< char >(&dir)->required(), "axis perpendicular to slab: x, y or z");
	optDesc.add_options()("box", value< vector<float> >(&box)->required()->multitoken(), "Box dimensions: x y z [A]");
	optDesc.add_options()("slab", value< size_t >(&nSlabs)->default_value(20), "number of slabs");
	optDesc.add_options()("sampling", value< size_t >(&sampling)->default_value(1000), "sampling [#]");
	optDesc.add_options()("atom", value< string >(&atom)->required(), "atom label to calculate density");

	variables_map options;
    store(parse_command_line(ac, av, optDesc), options);
	if(options.count("help"))
	{
		cout<<optDesc<<endl;
		exit(0);
	}
	notify(options);

	if(box.size() > 3) throw std::out_of_range("To many box values!");

	size_t direction;
	float slabArea;
	switch(dir)
	{
		case 'x': 
			{
				direction = 0;
				slabArea = box[1]*box[2];
			} break;
		case 'y':
			{
				direction = 1;
				slabArea = box[0]*box[2];
			} break;
		case 'z':
			{
				direction = 2;
				slabArea = box[0]*box[1];
			} break;
		default: throw std::out_of_range("Direction not valid!");
	}
	
	nSlabs++;
	const float dSlab = box[direction] / nSlabs;

	trajReader<xyzFrame> inTraj(inputFileName);
	cout<<"Total number of frames: "<<inTraj.nFrames()<<endl;

	vector< vector<size_t> > atomsPerSlabs;
	xyzFrame frame;
	while(inTraj>>frame)
	{
		if((inTraj.frameIndex() % 1000) == 0)
			cout<<"\rFrame: "<<inTraj.frameIndex()<<flush;

		if( ((inTraj.frameIndex()) % sampling) != 0 ) continue;

		vector<size_t> aSlab(nSlabs, 0); 
		for(size_t j = 0; j < nSlabs; j++)
		{
			for(size_t i = 0; i < frame.size(); ++i)
			{
				if(frame.atomLabel(i) != atom) continue;
				double xIn = j*dSlab - box[direction]/2.;
				if(frame[i][direction] >= xIn && frame[i][direction] < (xIn + dSlab) )
					aSlab[j]++;
			}
		}
		atomsPerSlabs.push_back(std::move(aSlab));
	}
	inTraj.close();

	for(size_t i = 0; i < atomsPerSlabs.size(); ++i)
	{
		ostringstream name;
		name<<"density"<<atom<<"_"<<dir<<"_"<<(i+1)*sampling<<".dat";
		
		ofstream outFile(name.str().c_str());
		outFile<<"#Distance [A]\tDensity [#/A^2]"<<endl;
		for(size_t j = 0; j < atomsPerSlabs[i].size(); ++j)
		{
			outFile<<fixed<<setw(10)<<setprecision(4)<<j*dSlab - box[direction]/2.<<"\t";
			outFile<<fixed<<setw(10)<<setprecision(4)<<atomsPerSlabs[i][j]/slabArea<<endl;
		}

		outFile.close();
	}

	cout<<endl<<"Done"<<endl;
}
