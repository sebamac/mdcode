#include <iostream>
#include <boost/program_options.hpp>

#include "xyzFrame.hpp"
#include "rw_conf.hpp"

using namespace std;
using namespace boost;
using namespace program_options;

int main(int ac, char *av[])
{
	string inputFileName, outFileName;
	size_t dumpFrame;

	options_description optDesc("Allowed options");
	optDesc.add_options()("help,h", "print help messages;");
	optDesc.add_options()("in-file,f", value< string >(&inputFileName)->required(), "input file;");
	optDesc.add_options()("out-file,o", value< string >(&outFileName)->default_value("out.xyz"), "output file name;");
	optDesc.add_options()("dump", value< size_t >(&dumpFrame)->required(), "extract 'dump' frame;");

	variables_map options;
    store(parse_command_line(ac, av, optDesc), options);
	if(options.count("help"))
	{
		cout<<optDesc<<endl;
		exit(0);
	}
	notify(options);

	trajReader inTraj(inputFileName);
	cout<<"Total number of frames: "<<inTraj.nFrames()<<endl;

	xyzFrame frame;
	while(inTraj>>frame)
	{
		if((inTraj.frameIndex() % 1000) == 0)
			cout<<"\rFrame: "<<inTraj.frameIndex()<<flush;

		if((inTraj.frameIndex() % dumpFrame) == 0)
		{
			cout<<endl<<"Saved frame "<<inTraj.frameIndex()<<"..."<<endl;
			frame.write(outFileName);
			break;
		}
	}
}