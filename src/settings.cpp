#include "settings.hpp"

Settings* Settings::settings_instance = nullptr;

Settings::Settings()
{
	
}
Settings::~Settings()
{
	if(settings_instance) delete settings_instance;
}

bool Settings::import(const string& file_name)
{
	ifstream file(file_name.c_str());
	
	if(!file)
	{
		cout<<"'"<<file_name<<"': no such file!"<<endl;
		return false;
	} 
	
	options_description optDesc("Allowed parameters");
	optDesc.add_options()("dt",     value< float >(&_time_step)->required() );
	optDesc.add_options()("nsteps", value< size_t >(&_nsteps)->required() );

	optDesc.add_options()("nstcomm", 	   value< size_t >(&_nstcomm)->default_value(1000) );
	optDesc.add_options()("nstxfinal",     value< size_t >(&_nstxfinal)->default_value(1000) );
	optDesc.add_options()("nstxout",       value< size_t >(&_nstxout)->default_value(1000) );
	optDesc.add_options()("nstvout",       value< size_t >(&_nstvout)->default_value(0) );
	optDesc.add_options()("nstfout",       value< size_t >(&_nstfout)->default_value(0) );
	optDesc.add_options()("nstlog",        value< size_t >(&_nstlog)->default_value(1000) );
	optDesc.add_options()("nstenergy",     value< size_t >(&_nstenergy)->default_value(1000) );
	optDesc.add_options()("nstout_comp",   value< size_t >(&_nstout_comp)->default_value(1000) );
	optDesc.add_options()("nstcalcenergy", value< size_t >(&_nstcalcenergy)->default_value(100) );

	optDesc.add_options()("tcoupl", value< string >(&_tcoupl)->default_value("no") );
	optDesc.add_options()("ref_t",  value< float >(&_ref_t)->default_value(-1) );
	optDesc.add_options()("t_seed", value< size_t >(&_t_seed)->default_value(std::chrono::system_clock::now().time_since_epoch().count()) );
	optDesc.add_options()("t_rate", value< float >(&_t_rate)->default_value(1) );
	optDesc.add_options()("t_increment",  value< float >(&_t_increment)->default_value(1) );
	optDesc.add_options()("ref_t_final",  value< float >(&_ref_t_final)->default_value(-1) );
	optDesc.add_options()("anderson_eta", value< float >(&_anderson_eta)->default_value(-1) );
	optDesc.add_options()("bussi_tau",    value< float >(&_bussi_tau)->default_value(-1) );

	optDesc.add_options()("pcoupl", value< string >(&_pcoupl)->default_value("no") );
	optDesc.add_options()("ref_p",  value< float >(&_ref_p)->default_value(-1) );
	 
	string temp_gen_vel; 
	optDesc.add_options()("gen_vel",  value< string >(&temp_gen_vel)->default_value("no") );
	optDesc.add_options()("gen_temp", value< float >(&_gen_temp)->default_value(-1) );
	optDesc.add_options()("gen_seed", value< size_t >(&_gen_seed)->default_value(1) );
	
	variables_map options;
	
	store(parse_config_file(file, optDesc), options);
	
	notify(options);
	
	file.close();
	
	_gen_vel = (temp_gen_vel == "yes");
	
	return true;
}
