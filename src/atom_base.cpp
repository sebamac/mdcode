#include "atom_base.hpp"

atomType::atomType() noexcept: _label(""), _mass(0), _LJsigma(0), _LJepsilon(0) 
{
	_LJsigma6 = 0.;
	_sqrtMass = 0.;
	_inverseMass = 0.;
}
atomType::atomType(const string label, const float mass, 
			  		const float sigma, const float epsilon) noexcept:
			  		 _label(label), _mass(mass), _LJsigma(sigma), _LJepsilon(epsilon) 
{
	_LJsigma6 = pow(_LJsigma, 6.);
	_sqrtMass = sqrt((double)_mass);
	_inverseMass = (double)1.0/_mass;
}
atomType::atomType(const atomType &a) noexcept
{
	_label 	   = a._label;
	_mass 	   = a._mass;
	_sqrtMass  = a._sqrtMass;
	_inverseMass = a._inverseMass;
	_LJsigma   = a._LJsigma;
	_LJsigma6  = a._LJsigma6;
	_LJepsilon = a._LJepsilon;
}

atomType& atomType::operator=(const atomType &a) noexcept
{
	_label 	   = a._label;
	_mass 	   = a._mass;
	_sqrtMass  = a._sqrtMass;
	_inverseMass = a._inverseMass;
	_LJsigma   = a._LJsigma;
	_LJsigma6  = a._LJsigma6;
	_LJepsilon = a._LJepsilon;

	return *this;
}

atomTypesList::atomTypesList(): _atomTypesIndex(nullptr)
{

}
atomTypesList::atomTypesList(const string fileName): _atomTypesIndex(nullptr)
{
	open(fileName);
}	
atomTypesList::atomTypesList(const string fileName, const xyzFrame &fr): _atomTypesIndex(nullptr)
{
	open(fileName);
	linkFrame(fr);
}
atomTypesList::~atomTypesList()
{
	if(_atomTypesIndex) delete[] _atomTypesIndex;
}

const atomType& atomTypesList::operator[](const string str) const
{
	return *find(_atomTypesVec.begin(), _atomTypesVec.end(), str);
}
const atomType& atomTypesList::at(const string str) const
{
	auto it(find(_atomTypesVec.begin(), _atomTypesVec.end(), str));
	
	if(it != _atomTypesVec.end()) return *it;

	ostringstream err;
	err<<"Atom species '"<<str<<"' not found in types list!";
	throw std::out_of_range(err.str());
}

bool atomTypesList::linkFrame(const xyzFrame &fr)
{
	if(_atomTypesVec.empty()) return false;
	
	_atomTypesIndex = new size_t[fr.size()];
	
	for(size_t i = 0; i < fr.size(); ++i)
	{
		auto it = find(_atomTypesVec.begin(), _atomTypesVec.end(), fr.atomLabel(i));
		
		if(it == _atomTypesVec.end())
		{
			ostringstream err;
			err<<"Atom species '"<<fr.atomLabel(i)<<"' not found in types list!";
			throw std::out_of_range(err.str());
		}
		
		_atomTypesIndex[i] = distance(_atomTypesVec.begin(), it);
	}
	
	return true;
}

bool atomTypesList::linkFrame(const string conf)
{
	xyzFrame fr(conf);
	
	if(!fr) return false;
	
	return linkFrame(fr);
}

//#Label sigma[A] epsilon[K] mass
void atomTypesList::open(const string type)
{
	_atomTypesVec.clear();

	ifstream file(type.c_str());

	if(!file) 
	{
		throw system_error(error_code(static_cast<int>(errc::no_such_file_or_directory),system_category()));
	}

	vector<string> lines;
	string stemp;
	while(getline(file, stemp))
	{
		if(stemp.empty()) continue;

		if(stemp[0] != '#') lines.push_back(stemp);
	}
	file.close();

	auto it(lines.begin());
	for(; it < lines.end(); ++it)
	{
		if(it->find("[type]") != string::npos || it->find("[ type ]") != string::npos)
		{
			++it;
			atomType atemp;
			do
			{
				istringstream str(*it);
				str>>atemp;
				_atomTypesVec.push_back(atemp);
				++it;
				if(it != lines.end()) if(it->find("[") != string::npos) break;
			} while(it != lines.end());
		}
	}

	sort(_atomTypesVec.begin(), _atomTypesVec.end());
	auto new_end = unique(_atomTypesVec.begin(), _atomTypesVec.end());
	if(new_end != _atomTypesVec.end()) _atomTypesVec.erase(new_end, _atomTypesVec.end());

	_epsilon.resize(_atomTypesVec.size(), vector< float >(_atomTypesVec.size(), 0));
	_sigma.resize  (_atomTypesVec.size(), vector< float >(_atomTypesVec.size(), 0));
	_sigma6.resize (_atomTypesVec.size(), vector< float >(_atomTypesVec.size(), 0));
		
	//save diagonal element
	for(size_t i = 0; i < _atomTypesVec.size(); ++i)
	{
		_epsilon[i][i] = _atomTypesVec.at(i)._LJepsilon;
		_sigma[i][i]   = _atomTypesVec.at(i)._LJsigma;
		_sigma6[i][i]  = _atomTypesVec.at(i)._LJsigma6;
	}

	for(; it < lines.end(); ++it)
	{
		if(it->find("[nonbonded_params]") != string::npos || it->find("[ nonbonded_params ]") != string::npos)
		{
			string l1,l2;
			float e,s;
			++it;
			do
			{
				istringstream str(*it);
				str>>l1>>l2>>e>>s;
	
				if(l1 == l2)
				{
					size_t p = distance(find(_atomTypesVec.begin(), _atomTypesVec.end(), l1), _atomTypesVec.begin());
					_epsilon[p][p] = e;
					_sigma[p][p] = s;
					_sigma6[p][p] = pow(s,6.);
				} else
				{
					size_t p1 = distance(find(_atomTypesVec.begin(), _atomTypesVec.end(), l1), _atomTypesVec.begin());
					size_t p2 = distance(find(_atomTypesVec.begin(), _atomTypesVec.end(), l2), _atomTypesVec.begin());
					_epsilon[p1][p2] = e;
					_sigma[p1][p2] = s;
					_sigma6[p1][p2] = pow(s,6.);
	
					_epsilon[p2][p1] = _epsilon[p1][p2];
					_sigma[p2][p1]   = _sigma[p1][p2]; 
					_sigma6[p2][p1]  = _sigma6[p1][p2]; 
				}
				if(it->find("[") != string::npos) break;
			}while(it != lines.end());
		}
	}

	//check and complete interaction matrix
	for(size_t i = 0; i < _atomTypesVec.size(); ++i)
	{
		for(size_t j = 0; j < _atomTypesVec.size(); ++j)
		{
			if(_epsilon[i][j] == 0)
				_epsilon[i][j] = sqrt(_epsilon[i][i] * _epsilon[j][j]);

			if(_sigma[i][j] == 0)
			{
				_sigma[i][j] = (_sigma[i][i] + _sigma[j][j])/2.;
				_sigma6[i][j] = pow(_sigma[i][j], 6.);
			}
		}
	}
}
