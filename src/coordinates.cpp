#include "coordinates.hpp"

tvec::tvec(): _size(0), _vec(nullptr)
{

}
tvec::tvec(const size_t& size): _size(size), _end(nullptr), _vec(nullptr)
{
	resize(_size);
}
tvec::tvec(const tvec& v):_size(0), _end(nullptr), _vec(nullptr)
{
	assign(v);
}
tvec::tvec(tvec&& v) noexcept: _size(0), _end(nullptr), _vec(nullptr)
{
	_vec = v._vec;
	v._vec = nullptr;
	
	_end = v._end;
	v._end = nullptr;
	
	std::swap(v._size, _size);
}
tvec::~tvec()
{
	clear();
}
tvec& tvec::operator=(const tvec& v)
{
	assign(v);
	
	return *this;
}
tvec& tvec::operator=(tvec&& v) noexcept
{
	if(_vec == v._vec) return *this;
	
	if(_vec) delete[] _vec;
	_vec = v._vec;
	v._vec = nullptr;
	
	_end = v._end;
	v._end = nullptr;
	
	_size = v._size;
	v._size = 0;
	
	return *this;
}

void tvec::clear()
{
	if(_vec) delete[] _vec;
	
	_vec = nullptr;
	_end = nullptr;
	_size = 0;
}

void tvec::reset() noexcept
{
	for(size_t i = 0; i < _size; i++)
	{
		_vec[i][0] = 0.;
		_vec[i][1] = 0.;
		_vec[i][2] = 0.;
	}
}

void tvec::assign(const tvec &v)
{
	if(_size != v._size)
	{
		clear();
		_size = v._size;
		
		_vec = new coordinate[_size];
		_end = _vec + _size;
	}
	
	copy(v);
}

void tvec::assign(tvec&& v) noexcept
{
	if(_vec == v._vec) return;
	
	clear();
	
	_vec = v._vec;
	v._vec = nullptr;
	
	_end = v._end;
	v._end = nullptr;
	
	_size = v._size;
	v._size = 0;
}

void tvec::copy(const tvec &v) noexcept
{
	for(size_t i=0; i<_size; i++)
	{
		_vec[i][0] = v._vec[i][0];
		_vec[i][1] = v._vec[i][1];
		_vec[i][2] = v._vec[i][2];
	}
}

void tvec::resize(const size_t& size)
{
	if(_size != size)
	{
		clear();
		_size = size;
	
		_vec = new coordinate[_size];
		_end = _vec + _size;
	}
		
	reset();
}

const coordinate& tvec::at(const size_t& i) const //lower
{
	if(i > _size) throw std::out_of_range ("out of range!");

	return _vec[i];
}

coordinate& tvec::at(const size_t& i) //lower
{
	if(i > _size) throw std::out_of_range ("out of range!");

	return _vec[i];
}
