#include "integrator.hpp"

integrator::integrator(Atoms *a, Settings *s) noexcept: timeStep(s->dt()), atoms(a)
{

}

velocityVerlet::velocityVerlet(Atoms *a, Settings *s)
	: integrator(a,s), 
	  dStep(timeStep / (2.0*amu2eV)), 
	  dStep2(timeStep * timeStep/(2.0*amu2eV))
{
	prevForces.resize(atoms->size());
}

void velocityVerlet::nextCoordinates() noexcept
{
	double dt2OverMass = 0;
#ifdef _OPENMP
	#pragma omp parallel for private(dt2OverMass)
#endif
	for(size_t i = 0; i < atoms->size(); ++i)
	{
		dt2OverMass = dStep2*atoms->atomTypesList::operator[](i).inverseMass();
		atoms->_vec[i][0] = atoms->_vec[i][0] + 
						    atoms->vvec[i][0]*timeStep + 
						    atoms->fvec[i][0]*dt2OverMass;
		
		atoms->_vec[i][1] = atoms->_vec[i][1] + 
						    atoms->vvec[i][1]*timeStep + 
						    atoms->fvec[i][1]*dt2OverMass;
		
		atoms->_vec[i][2] = atoms->_vec[i][2] + 
						    atoms->vvec[i][2]*timeStep + 
						    atoms->fvec[i][2]*dt2OverMass;
		
		prevForces[i][0] = atoms->fvec[i][0];
		prevForces[i][1] = atoms->fvec[i][1];
		prevForces[i][2] = atoms->fvec[i][2];
	}
}

void velocityVerlet::nextVelocities() noexcept
{
	double dtOverMass = 0;
#ifdef _OPENMP
	#pragma omp parallel for private (dtOverMass)
#endif
	for(size_t i = 0; i < atoms->size(); ++i)
	{
		dtOverMass = dStep*atoms->atomTypesList::operator[](i).inverseMass();
		atoms->vvec[i][0] = atoms->vvec[i][0] +
		   (prevForces[i][0] + atoms->fvec[i][0])*dtOverMass;
		
		atoms->vvec[i][1] = atoms->vvec[i][1] +
		   (prevForces[i][1] + atoms->fvec[i][1])*dtOverMass;
		
		atoms->vvec[i][2] = atoms->vvec[i][2] +
		   (prevForces[i][2] + atoms->fvec[i][2])*dtOverMass;
	}
}

thermostat::thermostat(const float& temp) noexcept: _temp(temp)
{

}
thermostat::~thermostat()
{
	
}

velocityVerletAnderson::velocityVerletAnderson(Atoms *a, Settings *s)
	: velocityVerlet(a,	s),
	  thermostat(s->ref_t()),
	  _eta(s->anderson_eta()*timeStep), 
	  _scaling(sqrt((long double)kBoltz_eV2K*_temp/amu2eV)),
	  _generator(s->t_seed()),
	  _gausGen(0.0, 1.0)
{

}

void velocityVerletAnderson::nextVelocities() noexcept
{
	double sigma = 0, dtOverMass = 0;
#ifdef _OPENMP
	#pragma omp parallel for private(sigma, dtOverMass)
#endif
	for(size_t i = 0; i < atoms->size(); ++i)
	{
		if(_uniformGen(_generator) <= _eta)
		{
			sigma = _scaling/atoms->atomTypesList::operator[](i).sqrtMass();
			atoms->vvec[i][0] = _gausGen(_generator) * sigma;
			atoms->vvec[i][1] = _gausGen(_generator) * sigma;
			atoms->vvec[i][2] = _gausGen(_generator) * sigma;
		} else
		{
			dtOverMass = dStep*atoms->atomTypesList::operator[](i).inverseMass();
			atoms->vvec[i][0] = atoms->vvec[i][0] +
		   	   (prevForces[i][0] + atoms->fvec[i][0])*dtOverMass;
		
			atoms->vvec[i][1] = atoms->vvec[i][1] +
		   	   (prevForces[i][1] + atoms->fvec[i][1])*dtOverMass;
		
			atoms->vvec[i][2] = atoms->vvec[i][2] +
		   	   (prevForces[i][2] + atoms->fvec[i][2])*dtOverMass;
		}
	}
}

void velocityVerletAnderson::setTemperature(const float& temp) noexcept
{
	_temp = temp;
	_scaling = sqrt((long double)(kBoltz_eV2K * _temp * inverse_amu2eV));
}


velocityVerletBussi::velocityVerletBussi(Atoms *a, Settings *s)
	: velocityVerlet(a,	s),
	  thermostat(s->ref_t()),
	  _dtOver2Tau(s->dt()/(2.0*s->bussi_tau())),
	  _K0(kBoltz_eV2K*a->_DOF*_temp/2.0),
	  _generator(s->t_seed()),
	  _gausGen(0.0, 1.0),
      _gammaGen((a->_DOF-1.0)/2.0, 2.0)
{
	_gausGen.reset();
	_gammaGen.reset();
}

void velocityVerletBussi::rescaleVelocity() noexcept
{
	double random_1 = _gausGen(_generator);
	double random2  = _gammaGen(_generator);
	double A = exp(-_dtOver2Tau);
	double B = _K0 / (atoms->_DOF * atoms->kinEnergy)*(1. - A*A);

	double alpha2 = A*A + B*(random_1*random_1 + random2) + 2.*A*sqrt(B)*random_1;
	double alpha  = sqrt(alpha2);

	for(size_t i = 0; i < atoms->size(); ++i)
	{
		atoms->vvec[i][0] *= alpha;
		atoms->vvec[i][1] *= alpha;
		atoms->vvec[i][2] *= alpha;
	}
}

void velocityVerletBussi::setTemperature(const float& temp) noexcept
{
	_temp = temp;
	_K0 = kBoltz_eV2K * atoms->_DOF * _temp * 0.5;
}
