#include "bufferedWriter.hpp"

bufferedWriter::bufferedWriter() noexcept:
									_size(0), _fileName(""), 
								  	_beg(nullptr), _end(nullptr), 
								  	_current(nullptr), _cp(nullptr)
{

}
bufferedWriter::bufferedWriter(const size_t& s, const string& name, const bool& append):
									_size(s), _fileName(name), 
									_beg(new string[_size]), _end(_beg+_size), 
									_current(_beg), _cp(new string[_size])
{
	ofstream file(_fileName.c_str(), append ? ios_base::app : ios_base::trunc);
	if(!file)
	{
		ostringstream str("bufferedWriter::bufferedWriter: '");
		str<<_fileName<<"': no such file!";
		throw fstream::failure(str.str());
	}
	file.close();
}

bufferedWriter::bufferedWriter(const size_t& s, const string& name, const string& firstLine, const bool& append):
									_size(s), _fileName(name), 
									_beg(new string[_size]), _end(_beg+_size), 
									_current(_beg), _cp(new string[_size])
{
	ofstream file(_fileName.c_str(), append ? ios_base::app : ios_base::trunc);
	if(!file)
	{
		ostringstream str("bufferedWriter::bufferedWriter: '");
		str<<_fileName<<"': no such file!";
		throw fstream::failure(str.str());
	}
	if(!append) file<<firstLine<<endl;
	file.close();
}

bufferedWriter::bufferedWriter(bufferedWriter&& bufWrt) noexcept: 
									_size(std::move(bufWrt._size)), _fileName(std::move(bufWrt._fileName)), 
									_beg(nullptr), _end(nullptr), _current(nullptr), _cp(nullptr), 
									_writeThread(std::move(bufWrt._writeThread)) 
{
	std::swap(_beg, bufWrt._beg);
	std::swap(_end, bufWrt._end);
	std::swap(_current, bufWrt._current);
	std::swap(_cp, bufWrt._cp);
}

bufferedWriter::~bufferedWriter()
{ 
	if(_beg) delete[] _beg;
	if(_cp)  delete[] _cp;
	
	if(_writeThread.joinable())
		_writeThread.join();
}
bufferedWriter& bufferedWriter::operator=(bufferedWriter&& bufWrt) noexcept
{
	_fileName.assign(std::move(bufWrt._fileName));

	if(_beg) delete[] _beg;
	if(_cp)  delete[] _cp;

	_beg = bufWrt._beg;
	_end = bufWrt._end;
	_cp  = bufWrt._cp;
	_current = bufWrt._current;

	bufWrt._beg = nullptr;
	bufWrt._end = nullptr;
	bufWrt._cp  = nullptr;
	bufWrt._current = nullptr;

	_size = bufWrt._size;
	bufWrt._size = 0;

	_writeThread = std::move(bufWrt._writeThread);

	return *this;
}

void bufferedWriter::write()
{	
	std::move(_beg, _end, _cp);
	
	ofstream file(_fileName.c_str(), ios_base::app);
	for(size_t i = 0; i < _size; ++i)
	{
		if(!_cp[i].empty())
		{
			file<<_cp[i];
			_cp[i].clear();
		}
	}
	
	file.close();
}

void bufferedWriter::writeThread()
{
	if(_writeThread.joinable())
		_writeThread.join();
	
	_writeThread = std::thread(&bufferedWriter::write, this);
}

bufferedWriter& bufferedWriter::operator<<(const string& str)
{
	*(_current++) = str;

	if(_current == _end)
	{
		writeThread();		
		_current = _beg;
	}

	return *this;
}
