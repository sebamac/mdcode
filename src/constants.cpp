#include "constants.h"

const double kBoltz_eV2K = 8.6173303e-5; // eV/K
const double kBoltz_K2eV = 1.1604522e+4; // K/eV
const double amu = 1.660539040e-27;      // kg/uma
const double eVtoJ = 1.602176565e-19;    // J/eV
const double JtoeV = 6.24150934e18;      // eV/J
const double amu2eV = 1.036427e-4; //(1e4*amu/eVtoJ);      //kg/J ~ 1e-4
const double inverse_amu2eV = 9.648533e+3;
 