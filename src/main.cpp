#include <iostream>
#include <iomanip>
#include <chrono>
#include <signal.h>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "atoms.hpp"
#include "settings.hpp"
#include "integrator.hpp"
#include "averager.hpp"
#include "logger.hpp"

#ifdef _OPENMP
#include <omp.h>
#endif

using namespace std;
using namespace std::chrono;
using namespace boost;
using namespace boost::program_options;

volatile bool SIGINT_flag = false;

void SIGINT_handler(int s);

std::string currentTime();
void backupFile(const string& name);
void checkFile(const string& name);

int main(const int ac, char *av[])
{
	string trajNameFile("traj.xyz"), finalNameFile("final.vxyz"), energyNameFile("energy.dat");
	string annealingNameFile("annealing.dat"), logNameFile("md.log"), trajVNameFile("traj.vxyz");
	size_t stepOut, nsteps = 0, initStep = 1;
	bool verbose, append;
#ifdef _OPENMP
	size_t numberTh;
#endif

	//for temperature annealing
	size_t nstChangeTemp = 0;
	float tIncrement = 0;

	Settings   *md_settings = Settings::get();
	thermostat *md_thermostat = nullptr;
	integrator *md_integrator = nullptr;
	Atoms md_atoms;

	{	
		string outputTrjBasename = "", parameterFile, inputCoordinateFile, atomTypeFile;

		options_description optDesc("Allowed parameters");
		optDesc.add_options()("coordinate,c", value< string >(&inputCoordinateFile)->required(), "input coordinate file .xyz");
		optDesc.add_options()("parameter,f", value< string >(&parameterFile)->required(), "input parameter file");
		optDesc.add_options()("atomtype,s", value< string >(&atomTypeFile)->required(), "atom parameters file");
		optDesc.add_options()("deffnm", value< string >(&outputTrjBasename), "base name for output files");
		optDesc.add_options()("stepout", value< size_t >(&stepOut)->default_value(1000), "number of steps for verbose output");
		optDesc.add_options()("verbose,v", bool_switch(&verbose)->default_value(false), "be verbose");
		optDesc.add_options()("append", bool_switch(&append)->default_value(false), "append to previous run;");
#ifdef _OPENMP
		optDesc.add_options()("ntomp", value< size_t >(&numberTh)->default_value(1), "number of openMP threads");
#endif
		optDesc.add_options()("help,h", "print help message.");
		
		variables_map options;
		store(parse_command_line(ac, av, optDesc), options);
		if(options.count("help"))
		{
			cout<<optDesc<<endl;
			exit(0);
		}
		notify(options);
	
		//ADD PREFIX TO FILES NAME
		if(!outputTrjBasename.empty())
		{
			outputTrjBasename+= '_'; 
			trajNameFile.insert(trajNameFile.begin(),     outputTrjBasename.begin(), outputTrjBasename.end());
			trajVNameFile.insert(trajVNameFile.begin(),   outputTrjBasename.begin(), outputTrjBasename.end());
			finalNameFile.insert(finalNameFile.begin(),   outputTrjBasename.begin(), outputTrjBasename.end());
			energyNameFile.insert(energyNameFile.begin(), outputTrjBasename.begin(), outputTrjBasename.end());
			annealingNameFile.insert(annealingNameFile.begin(), outputTrjBasename.begin(), outputTrjBasename.end());
			logNameFile.insert(logNameFile.begin(),       outputTrjBasename.begin(), outputTrjBasename.end());
		}
		if(filesystem::exists(logNameFile) && !append)
		{
			cout<<"Back-up '"<<logNameFile<<"' to 'old."<<logNameFile<<"'"<<endl;
			filesystem::rename(logNameFile, "old."+logNameFile);
		}
		LOGGER.open(logNameFile, append ? ios_base::app : ios_base::trunc);

		//SETTING OMP THREADS
#ifdef _OPENMP
		//omp_set_dynamic(0); // Explicitly disable dynamic teams
		omp_set_num_threads(numberTh); // Use nt threads for all consecutive parallel regions
		LOGGER<<"Setting "<<numberTh<<" threads openMP\n"<<endl;
#endif
		//CHECK FILES
		checkFile(parameterFile);
		checkFile(inputCoordinateFile);
		checkFile(atomTypeFile);

		//IMPORTING SETTINGS FILE
		cout<<endl<<"Importing md settings..."<<endl;
		LOGGER<<"Importing md settings from '"<<parameterFile<<"'"<<endl;
		md_settings->import(parameterFile);
		nsteps = md_settings->nsteps();
		LOGGER<<*md_settings<<endl;

		//IMPORTING INITIAL CONFIGURATION AND ATOM TYPES
		cout<<"\nImporting initial configuration and atom types..."<<endl;
		LOGGER<<"Importing atom types from '"<<atomTypeFile<<"'"<<endl;
		if(!append)
		{
			LOGGER<<"Importing initial configuration from '"<<inputCoordinateFile<<"'"<<endl;
			md_atoms.open(inputCoordinateFile, atomTypeFile);
		} else
		{
			//OPEN LAST .vxyz FRAME
			LOGGER<<"Importing initial configuration from '"<<finalNameFile<<"'"<<endl;
			md_atoms.open(finalNameFile, atomTypeFile);

			//EXTRACT CURRENT TIME
			istringstream cmt(md_atoms.commentLine());
			double temp;
			cmt>>temp>>temp;
			initStep = temp/md_settings->dt();

			if(initStep == md_settings->nsteps())
			{
				cout<<"ERROR: Continuation requested but no new step found"<<endl;
				LOGGER<<"ERROR: Continuation requested but no new step found in '"<<parameterFile<<"'"<<endl;
				exit(1);
			} else
			{
				cout<<"Request continuation. Init. step: "<<initStep<<", final step: "<<md_settings->nsteps()<<endl;
				LOGGER<<"Request continuation. Init. step: "<<initStep<<", final step: "<<md_settings->nsteps()<<endl;
			}
			initStep++;
		}
		
		cout<<"Found "<<md_atoms.nspecies()<<" species: ";
		LOGGER<<"\nFound "<<md_atoms.nspecies()<<" species:"<<endl;
		LOGGER<<"(Name\tEpsilon[eV]\tSigma[A]\tMass[amu])"<<endl;
		for(size_t i = 0; i < md_atoms.nspecies(); i++)
		{
			cout<<md_atoms.atom(i).label()<<", ";
			LOGGER<<md_atoms.atom(i)<<endl;
		}
		cout<<endl;
		LOGGER<<"\nNon-bonded interaction matrix:"<<endl;
		LOGGER<<"(Atom1\tAtom2\tEpsilon[eV]\tSigma[A])"<<endl;
		for(size_t i = 0; i < md_atoms.nspecies(); i++)
		{
			for(size_t j = 0; j < md_atoms.nspecies(); j++)
			{
				LOGGER<<md_atoms.atom(i).label()<<" "<<md_atoms.atom(j).label()<<" "<<md_atoms.LJ_matrix(i, j).first<<" "<<md_atoms.LJ_matrix(i, j).second<<endl;
			}
		}
		cout<<"Total number of atoms: "<<md_atoms.natoms()<<endl<<endl;
		LOGGER<<"Total number of atoms: "<<md_atoms.natoms()<<endl;

		//INITIALIZING VELOCITIES (needed by integrator!)
		if(md_settings->gen_vel() && !append)
		{
			if(md_settings->gen_temp() == -1)
			{
				cout<<"Generate	velocities was requested but 'gen_temp' must be greater than zero!"<<endl;
				LOGGER<<"\nERROR: Generate velocities was requested but 'gen_temp' must be greater than zero!"<<endl;
				exit(1);
			}
			cout<<"Initializing velocities with initial temperature "<<md_settings->gen_temp()<<" K"<<endl<<endl;
			LOGGER<<"Initializing velocities with initial temperature "<<md_settings->gen_temp()<<" K"<<endl<<endl;
			md_atoms.initVelocities(md_settings->gen_temp(), md_settings->gen_seed());
		} else //check if velocities are present
		{
			if(md_atoms.velocities().empty())
			{
				cout<<"Velocities not present in the starting configuration but needed!"<<endl;
				LOGGER<<"\nERROR: Velocities not present in the starting configuration but needed!"<<endl;
				exit(1);
			} else
			{
				cout<<"Initial velocities taken from input configuration file..."<<endl;
				LOGGER<<"Initial velocities taken from input configuration file..."<<endl;
			}
		}

		//CREATING INTEGRATOR
		if(md_settings->tcoupl() == "no") 
			md_integrator = new velocityVerlet(&md_atoms, md_settings);
		else 
		{
			if (md_settings->ref_t() <= 0 )
			{
				cout<<"Anderson thermostat was requested but 'ref_t' must be greater then zero!"<<endl;
				LOGGER<<"ERROR: Anderson thermostat was requested but 'ref_t' must be greater then zero!"<<endl;
				exit(1);
			}

			if(md_settings->tcoupl() == "anderson")
			{
				if(md_settings->anderson_eta() == -1)
				{
					cout<<"Anderson thermostat was requested but missing parameters!"<<endl;
					LOGGER<<"ERROR: Anderson thermostat was requested but missing parameters!"<<endl;
					exit(1);
				}
				
				md_integrator = new velocityVerletAnderson(&md_atoms, md_settings);
				md_thermostat = dynamic_cast<thermostat*>(md_integrator);
				cout<<"Set temperature coupling with Anderson thermostat at "<<md_settings->ref_t()<<" K"<<endl;
				LOGGER<<"Set temperature coupling with Anderson thermostat at "<<md_settings->ref_t()<<" K"<<endl;
			} else if(md_settings->tcoupl() == "bussi")
			{
				if(md_settings->bussi_tau() == -1)
				{
					cout<<"Bussi v. rescale thermostat was requested but missing parameters!"<<endl;
					LOGGER<<"ERROR: Bussi v. rescale thermostat was requested but missing parameters!"<<endl;
					exit(1);
				}
				
				md_integrator = new velocityVerletBussi(&md_atoms, md_settings);
				md_thermostat = dynamic_cast<thermostat*>(md_integrator);
				cout<<"Set temperature coupling with Bussi thermostat at "<<md_settings->ref_t()<<" K"<<endl;
				LOGGER<<"Set temperature coupling with Bussi thermostat at "<<md_settings->ref_t()<<" K"<<endl;
			} else
			{
				cout<<"'"<<md_settings->tcoupl()<<"' thermostat not found! The available are \n";
				LOGGER<<"ERROR: '"<<md_settings->tcoupl()<<"' thermostat not found!";
				exit(1);
			}
		}

		//CHECK FOR TEMPERATURE ANNEALING
		if(md_settings->ref_t_final() > 0)
		{
			if(append)
			{
				cout<<"ERROR: Continuation is not supported during temperature annealing! Do not use 'append' options!"<<endl;
				LOGGER<<"ERROR: Continuation is not supported during temperature annealing! Do not use 'append' options!"<<endl;
				exit(1);
			}
			if(md_settings->ref_t_final() == md_settings->ref_t())
			{
				cout<<"Temperature annealing was requested but 'ref_t' and 'ref_t_final' can not be equal!"<<endl;
				LOGGER<<"ERROR: Temperature annealing was requested but 'ref_t' and 'ref_t_final' can not be equal!"<<endl;
				exit(1);
			}
			if(md_settings->tcoupl() == "no")
			{
				cout<<"Temperature annealing was requested but none thermostat selected!"<<endl;
				LOGGER<<"Temperature annealing was requested but none thermostat selected!"<<endl;
				exit(1);
			}
			if(md_settings->t_rate() <= 0)
			{
				cout<<"Temperature annealing was requested but 't_rate' must be greater then zero!"<<endl;
				LOGGER<<"ERROR: Temperature annealing was requested but 't_rate' must be greater then zero!"<<endl;
				exit(1);
			}
			if(md_settings->t_increment() == 0)
			{
				cout<<"Temperature annealing was requested but 't_increment' can not be equal to zero!"<<endl;
				LOGGER<<"ERROR: Temperature annealing was requested but 't_increment' can not be equal to zero!"<<endl;
				exit(1);
			}

			//Every 'nstChangeTemp' change temp of 'md_settings->t_increment()'
			nstChangeTemp = md_settings->t_rate()*1e3 / md_settings->dt(); 

			tIncrement = md_settings->t_increment();
			if(md_settings->ref_t_final() < md_settings->ref_t()) tIncrement = -tIncrement;

			nsteps = (fabs(md_settings->ref_t() - md_settings->ref_t_final()) + md_settings->t_increment()) 
				  			/ md_settings->t_increment() * nstChangeTemp;
			
			cout<<"Temperature annealing requested:"<<endl;
			LOGGER<<"Temperature annealing requested:"<<endl;
			if(md_settings->t_increment() > 1)
			{
				cout<<"WARNING: 't_increment' is greater then 1 K; is that correct?"<<endl;
				LOGGER<<"\nWARNING: 't_increment' is greater then 1 K; is that correct?"<<endl;
			}
			cout<<"\tInitial temp.: "<<md_settings->ref_t()<<" K"<<endl;
			cout<<"\tFinal temp.:   "<<md_settings->ref_t_final()<<" K"<<endl;
			cout<<"\tTotal steps:   "<<nsteps<<" steps ( "<<md_settings->dt()*nsteps<<" ps )"<<endl<<endl;
			
			LOGGER<<"\tInitial temp.: "<<md_settings->ref_t()<<" K"<<endl;
			LOGGER<<"\tFinal temp.:   "<<md_settings->ref_t_final()<<" K"<<endl;
			LOGGER<<"\tTotal steps:   "<<nsteps<<" steps ( "<<md_settings->dt()*nsteps<<" ps )"<<endl<<endl;
		}

		//FILES BACKUP
		if(!append)
		{
			backupFile(trajNameFile);
			backupFile(trajVNameFile);
			backupFile(finalNameFile);
			backupFile(energyNameFile);
			backupFile(annealingNameFile);
		}
	}
	
	md_atoms.evalForces();
	md_atoms.calcKineticEnergy();

	cout<<endl;
	cout<<"Initial temperature:      "<<md_atoms.temperature()<<" K"<<endl;
	cout<<"Initial potential energy: "<<md_atoms.potentialEnergy()<<" eV"<<endl;
	cout<<"Initial kinetic energy:   "<<md_atoms.kineticEnergy()<<" eV"<<endl;
	cout<<"Total initial energy:     "<<md_atoms.kineticEnergy() + md_atoms.potentialEnergy()<<" eV"<<endl;
	cout<<"Max force:                "<<md_atoms.fMax()*1e3<<" meV/A"<<endl;
	LOGGER<<"\nInitial temperature:      "<<md_atoms.temperature()<<" K"<<endl;
	LOGGER<<"Initial potential energy: "<<md_atoms.potentialEnergy()<<" eV"<<endl;
	LOGGER<<"Initial kinetic energy:   "<<md_atoms.kineticEnergy()<<" eV"<<endl;
	LOGGER<<"Total initial energy:     "<<md_atoms.kineticEnergy() + md_atoms.potentialEnergy()<<" eV"<<endl;
	LOGGER<<"Max force:                "<<md_atoms.fMax()*1e3<<" meV/A"<<endl;
	
	energyWriter energyFile(energyNameFile, append);
	LOGGER<<"Opened '"<<energyNameFile<<"' energy file"<<endl;
	trajWriter trajFile(md_atoms.natoms(), trajNameFile, append);
	LOGGER<<"Opened '"<<trajNameFile<<"' trajectory file"<<endl;
	
	trajVWriter *trajVFile = nullptr;
	if(md_settings->nstvout() > 0)
	{
		trajVFile = new trajVWriter(md_atoms.natoms(), trajVNameFile, append);
		LOGGER<<"Opened '"<<trajVNameFile<<"' velocity-trajectory file"<<endl;
	}
	
	averager<double> averageEnergy(md_settings->nstenergy()/md_settings->nstcalcenergy());
	averager<float>  averageTemp(md_settings->nstenergy()/md_settings->nstcalcenergy());

	energyWriter     *annealingFile   = nullptr;
	averager<double> *annealingEnergy = nullptr;
	averager<float>  *annealingTemp   = nullptr;
	if(nstChangeTemp > 0)
	{
		annealingFile = new energyWriter(annealingNameFile, append, 15); //write to file every 15 lines
		LOGGER<<"Opened '"<<annealingNameFile<<"' annealing file"<<endl;

		annealingEnergy = new averager<double>(nstChangeTemp/md_settings->nstcalcenergy());
		annealingTemp = new averager<float>(nstChangeTemp/md_settings->nstcalcenergy());
	}
	
	cout<<endl<<currentTime();
	cout<<"Starting mdrun: "<<nsteps<<" steps ("<<nsteps*md_settings->dt()<<" ps)";
	LOGGER<<"\n\n"<<currentTime()<<" starting mdrun: "<<nsteps<<" steps ("<<nsteps*md_settings->dt()<<" ps)"<<endl;
	if(append)
	{
		cout<<", continuation from "<<initStep<<" ("<<initStep*md_settings->dt()<<" ps)";
		LOGGER<<"Continuation from "<<initStep<<" ("<<initStep*md_settings->dt()<<" ps)"<<endl;
	}
	cout<<"\n\n"<<flush;
	
	signal(SIGINT,  SIGINT_handler);
	signal(SIGTERM, SIGINT_handler);
	signal(SIGKILL, SIGINT_handler);
	
	size_t step = 1;
	high_resolution_clock::time_point startTime = high_resolution_clock::now();
	for(step = initStep; step <= nsteps; ++step)
	{
		//Integrate equation of motion and apply thermostat (if needed)
		md_integrator->integrate();
		//Center of Mass motion removal
		if(md_settings->nstcomm() > 0) if((step % md_settings->nstcomm()) == 0)
		{
			md_atoms.commRemoval();
		}
		//Calculate the kinetic energy and temperature
		md_atoms.calcKineticEnergy();
		
		if(md_settings->nstcalcenergy() > 0) if((step % md_settings->nstcalcenergy()) == 0)
		{			
			averageEnergy.push_back(md_atoms.kineticEnergy() + md_atoms.potentialEnergy());
			averageTemp.push_back(md_atoms.temperature());

			if(nstChangeTemp > 0)
			{
				annealingEnergy->push_back(md_atoms.kineticEnergy() + md_atoms.potentialEnergy());
				annealingTemp->push_back(md_atoms.temperature());
			}
		}

		if(md_settings->nstenergy() > 0) if((step % md_settings->nstenergy()) == 0)
		{
			energyFile.write(step*md_settings->dt(),
						averageEnergy.average(), averageTemp.average());
		}

		if(md_settings->nstxfinal() > 0) if((step % md_settings->nstxfinal()) == 0)
		{
			md_atoms.write(finalNameFile, step*md_settings->dt());
		}
	
		if(md_settings->nstxout() > 0) if((step % md_settings->nstxout()) == 0)
		{
			trajFile<<md_atoms;
		}

		if(md_settings->nstvout() > 0) if((step % md_settings->nstvout()) == 0)
		{
			(*trajVFile)<<md_atoms;
		}
		//temp. annealing
		if(nstChangeTemp > 0) if((step % nstChangeTemp) == 0)
		{
			md_thermostat->setTemperature( md_thermostat->temperature() + tIncrement );

			annealingFile->write(step*md_settings->dt(),
						annealingEnergy->average(), annealingTemp->average());
		}

		if(md_settings->nstlog() > 0) if((step % md_settings->nstlog()) == 0)
		{
			high_resolution_clock::time_point t2 = high_resolution_clock::now();
			LOGGER<<"\nStep: "<<step<<" Time: "<<step*md_settings->dt()<<" ps\n";
			LOGGER<<fixed<<setprecision(8)<<setw(11)<<"Energy: "<<md_atoms.potentialEnergy()+md_atoms.kineticEnergy()<<" eV ";
			LOGGER<<"Max force: "<<md_atoms.fMax()*1e3<<" meV/A ";
			LOGGER<<"Temperature: "<<md_atoms.temperature()<<" K\n";
			LOGGER<<"   Performance: "<<((step-initStep)*md_settings->dt()/duration_cast<seconds>(t2 - startTime).count())*3.6<<" ns/h"<<endl;
		}

		if(verbose)	if((step % stepOut) == 0)
		{
			cout<<"\rStep: "<<step;
			cout<<fixed<<setprecision(8)<<setw(11)<<" E="<<md_atoms.potentialEnergy() + md_atoms.kineticEnergy()<<" eV, Max force="<<md_atoms.fMax()*1e3<<" meV/A ";
		}
		
		if(SIGINT_flag) break;
	}
	high_resolution_clock::time_point stopTime = high_resolution_clock::now();
	int duration = duration_cast<seconds>(stopTime - startTime).count();
	float totalTime = (step-initStep)*md_settings->dt(); // ps

	trajFile.flush();
	energyFile.flush();
	if(annealingFile) annealingFile->flush();
	
	if(SIGINT_flag) 
	{
		cout<<"\nStopped due to SIGINT!"<<endl;
		LOGGER<<"\n!md_run was stopped due to a SIGINT interrupt!"<<endl;
	}
	cout<<currentTime()<<endl;
	cout<<"Writing final configuration...\n";
	cout<<"Finished md run:   "<<fixed<<setprecision(2)<<(totalTime/duration)*3.6<<" ns/h"<<endl;
	cout<<"Final energy:      "<<md_atoms.potentialEnergy()+md_atoms.kineticEnergy()<<" eV"<<endl;
	cout<<"Final temperature: "<<md_atoms.temperature()<<" K"<<endl;
	cout<<"Max force:         "<<md_atoms.fMax()<<" eV/A\n"<<endl;
	LOGGER<<"\n"<<currentTime()<<endl;
	LOGGER<<"Writing final configuration...\n";
	LOGGER<<"\nFinished md run:   "<<fixed<<setprecision(2)<<(totalTime/duration)*3.6<<" ns/h"<<endl;
	LOGGER<<"Final energy:      "<<md_atoms.potentialEnergy()+md_atoms.kineticEnergy()<<" eV"<<endl;
	LOGGER<<"Final temperature: "<<md_atoms.temperature()<<" K"<<endl;
	LOGGER<<"Max force:         "<<md_atoms.fMax()<<" eV/A\n"<<endl;
	
	md_atoms.write(finalNameFile,  totalTime);
	
	LOGGER.close();
	
	delete md_integrator;

	if(trajVFile)       delete trajVFile;
	if(annealingFile)   delete annealingFile;
	if(annealingTemp)   delete annealingTemp;
	if(annealingEnergy) delete annealingEnergy;
}

void SIGINT_handler(int s)
{
	SIGINT_flag = true;
}

void backupFile(const string& name)
{
	if(filesystem::exists(name))
	{
		cout<<"Back-up '"<<name<<"' to 'old."<<name<<"'"<<endl;
		LOGGER<<"Back-up '"<<name<<"' to 'old."<<name<<"'"<<endl;
		filesystem::rename(name, "old."+name);
	}
}

void checkFile(const string& name)
{
	if(!filesystem::exists(name))
	{
		cout<<"'"<<name<<"': no such file!\nAborting...\n"<<endl;
		LOGGER<<"ERROR: '"<<name<<"': no such file!"<<endl;
		exit(1);
	}
}

std::string currentTime()
{
    std::chrono::system_clock::time_point p = std::chrono::system_clock::now();

    std::time_t t = std::chrono::system_clock::to_time_t(p);
    std::ostringstream str;
    str<<std::ctime(&t); // for example : Tue Sep 27 14:21:13 2011

    return str.str();
}
