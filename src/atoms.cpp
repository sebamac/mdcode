#include "atoms.hpp"

using namespace std;
using namespace std::chrono;

Atoms::Atoms(const string conf, const string type): vxyzFrame(), atomTypesList()
{
	Atoms::open(conf, type);
}
void Atoms::open(const string conf, const string type)
{
	if(conf.find(".xyz") != string::npos)
		vxyzFrame::operator=(std::move(xyzFrame(conf)));
	else vxyzFrame::open(conf);

	atomTypesList::open(type);
	atomTypesList::linkFrame(*this);
	
	fvec.resize(xyzFrame::size());

	//Calculate system mass
	mass = 0;
	for(size_t i = 0; i < xyzFrame::size(); ++i)
	{
		mass += atomTypesList::at(i).mass();
	}
	_inverseMass = (long double)1.0/mass;
	
	//put the origin at cenetr of mass
	// coordinate com;
// 	centerOfMass(com);
// 	for(size_t i = 0; i < xyzFrame::size(); ++i)
// 	{
// 		_vec[i][0] -= com[0];
// 		_vec[i][1] -= com[1];
// 		_vec[i][2] -= com[2];
// 	}
	
}
void Atoms::write(const string fileName, const float time)
{
	char buf[33];
	snprintf(buf, 32, "%13.8f %9.3e", kinEnergy + potEnergy, time);
	xyzFrame::commentLine(string(buf));

	vxyzFrame::write(fileName);
}

void Atoms::initVelocities(const float temp, const unsigned int seed) // A/ps
{
	{
		//Extract the initial velocities from a normal distribution!
		std::mt19937_64 generator(seed);
		std::normal_distribution<double> gausGen(0.0,1.);
		for(size_t i = 0; i < xyzFrame::size(); ++i)
		{
			vvec[i][0] = gausGen(generator);
			vvec[i][1] = gausGen(generator);
			vvec[i][2] = gausGen(generator);		
		}
	}
	//REMOVE linear momentum
	commRemoval();

	//REMOVE angular momentum
	//array< array<double, 3>, 3 > inverseInMat = invertedInertiaMatrix();

	// coordinate com, angMomentum;
	// centerOfMass(com);
	// do
	// {
	// 	angularMomentum(angMomentum);
	// 	coordinate angVel = {0,0,0};
	// 	for(size_t i = 0; i < 3; ++i)
	// 	{
	// 		angVel[i] = ( inverseInMat[i][0]*angMomentum[0] +
	// 					  inverseInMat[i][1]*angMomentum[1] +
	// 					  inverseInMat[i][2]*angMomentum[2] );
	// 	}

	// 	for(size_t i = 0; i < xyzFrame::size(); ++i)
	// 	{
	// 		coordinate vt = {0,0,0};
	// 		coordinate pos = { _vec[i][0] - com[0],
	// 						   _vec[i][1] - com[1],
	// 						   _vec[i][2] - com[2] };
	// 		crossProduct(angVel, pos, vt);

	// 		vvec[i][0] -= vt[0];
	// 		vvec[i][1] -= vt[1];
	// 		vvec[i][2] -= vt[2];
	// 	}
	// }while(mod2(angMomentum) >= 1e-10);

	//!!!WARNING!!!
	//SELECT HERE THE CORRECT DEGREE OF FREEDOM!!!
	_DOF = 3*xyzFrame::size() - 3;

	//scaling to target kinetic energy
	calcKineticEnergy();
	const double K0 = 0.5*kBoltz_eV2K*_DOF*temp; //eV
	const double alpha = sqrt( 2.*K0/kinEnergy );
	for(size_t i = 0; i < xyzFrame::size(); ++i)
	{
		vvec[i][0] *= alpha;
		vvec[i][1] *= alpha;
		vvec[i][2] *= alpha;
	}
	calcKineticEnergy();
}

void Atoms::evalForces()
{
	potEnergy = 0;
	fvec.reset();
	const size_t size = xyzFrame::size();

#ifndef _OPENMP //single thread
	double rij2, ep, u6, fij;
	size_t i = 0, j = 0;
	for(i = 0; i < size; ++i)
	{
		for(j = i + 1; j < size; ++j)
		{
			coordinate rij = {_vec[i][0] - _vec[j][0],
					 		  _vec[i][1] - _vec[j][1],
							  _vec[i][2] - _vec[j][2]};
			rij2 = mod2(rij); //it's an inline function!
				
			//potential energy
			ep = epsilon(i, j);
			u6 = sigma6(i, j)/(rij2*rij2*rij2);
			potEnergy +=  4.*ep*u6*(u6 - 1.); // eV
						
			//forces
			fij = 48.*ep*u6*(u6 - 0.5)/rij2; // eV/A^2
			
			fvec[i][0] +=  fij*rij[0];		 // eV/A
			fvec[i][1] +=  fij*rij[1];
			fvec[i][2] +=  fij*rij[2];
			
			fvec[j][0] -=  fij*rij[0];
			fvec[j][1] -=  fij*rij[1];
			fvec[j][2] -=  fij*rij[2];
		}
	}
#else
	size_t i=0,j=0;
	#pragma omp parallel for private(i,j) shared(fvec) reduction(+:potEnergy)
	for(i = 0; i < size; ++i)
	{
		for(j = i+1; j < size; ++j)
		{
			//if(i == j) continue;
			
			coordinate rij = {_vec[i][0] - _vec[j][0],
					 		  _vec[i][1] - _vec[j][1],
							  _vec[i][2] - _vec[j][2]};
			double rij2 = mod2(rij); //it's an inline function!
				
			//potential energy
			double ep = epsilon(i, j);
			double u6 = sigma6(i, j)/(rij2*rij2*rij2);
					
			//forces
			double fij = 48.*ep*u6*(u6 - 0.5)/rij2; // eV/A
			
			potEnergy  +=  4.*ep*u6*(u6 - 1.); // eV
			fvec[i][0] +=  fij*rij[0];
			fvec[i][1] +=  fij*rij[1];
			fvec[i][2] +=  fij*rij[2];

			fvec[j][0] -=  fij*rij[0];
			fvec[j][1] -=  fij*rij[1];
			fvec[j][2] -=  fij*rij[2];
		}
	}
#endif
}

void Atoms::commRemoval() //openMP is useless here (tested)
{
	coordinate linM; //A * uma / ps
	linearMomentum(linM);
	linM[0] *= _inverseMass;
	linM[1] *= _inverseMass;
	linM[2] *= _inverseMass;
	
	double kinInit = 0, kinFin = 0;
	for(size_t i = 0; i < xyzFrame::size(); ++i)
	{
		kinInit += mod2(vvec[i]) * atomTypesList::operator[](i).mass();
		
		vvec[i][0] -= linM[0];
		vvec[i][1] -= linM[1];
		vvec[i][2] -= linM[2];

		kinFin += mod2(vvec[i]) * atomTypesList::operator[](i).mass();
	}
	
	//rescale to the initial kinetic energy;
	const double alpha = kinInit / kinFin;
	for(size_t i = 0; i < xyzFrame::size(); ++i)
	{
		vvec[i][0] *= alpha;
		vvec[i][1] *= alpha;
		vvec[i][2] *= alpha;
	}
}

void Atoms::rotmRemoval() //WARNING: To do ONLY after Atoms::commRemoval()!!!
{
	array< array<double, 3>, 3 > inverseInMat(invertedInertiaMatrix());
	coordinate com, angMomentum, vt;
	centerOfMass(com);
	angularMomentum(angMomentum);

	//L(i) = I(ij)w(j)
	coordinate angVel = { ( inverseInMat[0][0]*angMomentum[0] + 
				   		    inverseInMat[0][1]*angMomentum[1] +
				  			inverseInMat[0][2]*angMomentum[2] ),
						  ( inverseInMat[1][0]*angMomentum[0] + 
				   			inverseInMat[1][1]*angMomentum[1] +
				  			inverseInMat[1][2]*angMomentum[2] ),
				          ( inverseInMat[2][0]*angMomentum[0] + 
				   			inverseInMat[2][1]*angMomentum[1] +
				   			inverseInMat[2][2]*angMomentum[2] ) };

	for(size_t i = 0; i < xyzFrame::size(); ++i)
	{
		
		coordinate pos = { _vec[i][0] - com[0], 
						   _vec[i][1] - com[1], 
						   _vec[i][2] - com[2] };
		crossProduct(angVel, pos, vt);

		vvec[i][0] = vvec[i][0] - vt[0];
		vvec[i][1] = vvec[i][1] - vt[1];
		vvec[i][2] = vvec[i][2] - vt[2];
	}
}

double Atoms::fMax() //openMP is useless here
{
	double fMax = -1, ff2 = 0; //in mod!!!
	for(size_t i = 0; i < xyzFrame::size(); ++i)
	{
		ff2 = mod2(fvec[i]);
		if(ff2 > fMax) fMax = ff2;
	}
	
	return sqrt(fMax);
}

void Atoms::calcKineticEnergy() //openMP is useless here
{
	kinEnergy = 0;
	for(size_t i = 0; i < size(); ++i)
		kinEnergy+= atomTypesList::operator[](i).mass() * mod2(vvec[i]);
	
	kinEnergy = kinEnergy * amu2eV * 0.5; // eV
	
	_temperature =  2. * kinEnergy * kBoltz_K2eV / _DOF ; // K
}

void Atoms::centerOfMass(coordinate &com)
{
	com[0] = com[1] = com[2] = 0.;
	for(size_t i = 0; i < xyzFrame::size(); ++i)
	{
		com[0] += _vec[i][0]*atomTypesList::operator[](i).mass();
		com[1] += _vec[i][1]*atomTypesList::operator[](i).mass();
		com[2] += _vec[i][2]*atomTypesList::operator[](i).mass();
	}
	com[0] *= _inverseMass;
	com[1] *= _inverseMass;
	com[2] *= _inverseMass;
}

void Atoms::linearMomentum(coordinate &lin)
{
	lin[0] = lin[1] = lin[2] = 0.;
	for(size_t i = 0; i < xyzFrame::size(); ++i)
	{
		lin[0] += vvec[i][0] * atomTypesList::operator[](i).mass();
		lin[1] += vvec[i][1] * atomTypesList::operator[](i).mass();
		lin[2] += vvec[i][2] * atomTypesList::operator[](i).mass();
	}
}

void Atoms::angularMomentum(coordinate &angMomentum)
{
	angMomentum[0] = angMomentum[1] = angMomentum[2] = 0.;
	coordinate com, temp = {0,0,0};
	centerOfMass(com);
	for(size_t i = 0; i < xyzFrame::size(); ++i)
	{
		coordinate pos = { _vec[i][0] - com[0], 
						   _vec[i][1] - com[1], 
						   _vec[i][2] - com[2] };

		crossProduct(pos, vvec[i], temp);
		angMomentum[0] += temp[0]*atomTypesList::operator[](i).mass();
		angMomentum[1] += temp[1]*atomTypesList::operator[](i).mass();
		angMomentum[2] += temp[2]*atomTypesList::operator[](i).mass();
	}
}

array< array<double, 3>, 3> Atoms::inertiaMatrix()
{
	array< array<double, 3>, 3> mat;
	mat.fill({0,0,0});

	coordinate com = {0,0,0};
	centerOfMass(com);
	
	for(size_t i = 0; i < xyzFrame::size(); ++i)
	{
		coordinate pos = { _vec[i][0] - com[0], 
						   _vec[i][1] - com[1], 
						   _vec[i][2] - com[2] };

		mat[0][0] += (pos[1]*pos[1] + pos[2]*pos[2])*atomTypesList::operator[](i).mass();
		mat[1][1] += (pos[0]*pos[0] + pos[2]*pos[2])*atomTypesList::operator[](i).mass();
		mat[2][2] += (pos[0]*pos[0] + pos[1]*pos[1])*atomTypesList::operator[](i).mass();

		mat[0][1] -=  pos[0]*pos[1]*atomTypesList::operator[](i).mass();
		mat[0][2] -=  pos[0]*pos[2]*atomTypesList::operator[](i).mass();
		mat[1][2] -=  pos[1]*pos[2]*atomTypesList::operator[](i).mass();
	}

	mat[1][0] = mat[0][1];
	mat[2][0] = mat[0][2];
	mat[2][1] = mat[1][2];

	return mat;
}

array< array<double, 3>, 3> Atoms::invertedInertiaMatrix()
{
	array< array<double, 3>, 3> input(inertiaMatrix());
	array< array<double, 3>, 3> inverse;
	inverse.fill({0,0,0});
	
	inverse[0][0] = input[1][1]*input[2][2] - input[1][2]*input[2][1];
	inverse[1][0] = input[1][2]*input[2][0] - input[1][0]*input[2][2];
	inverse[2][0] = input[1][0]*input[2][1] - input[1][1]*input[2][0];
	inverse[0][1] = input[0][2]*input[2][1] - input[0][1]*input[2][2];
	inverse[1][1] = input[0][0]*input[2][2] - input[0][2]*input[2][0];
	inverse[2][1] = input[0][1]*input[2][0] - input[0][0]*input[2][1];
	inverse[0][2] = input[0][1]*input[1][2] - input[0][2]*input[1][1];
	inverse[1][2] = input[0][2]*input[1][0] - input[0][0]*input[1][2];
	inverse[2][2] = input[0][0]*input[1][1] - input[0][1]*input[1][0];
	
	double overDet = 1.0/(input[0][0]*inverse[0][0] + input[0][1]*inverse[1][0] + input[0][2]*inverse[2][0]);
	
	for(auto &i : inverse)
		for(auto &j : i)
			j *= overDet;

	return inverse;
}
