#include "xyzFrame.hpp"

xyzFrame::xyzFrame() noexcept: aLabels(nullptr), _commentLine()
{

}
xyzFrame::xyzFrame(const string& fileName): aLabels(nullptr), _commentLine()
{
	open(fileName);
}
xyzFrame::xyzFrame(const xyzFrame& fr): tvec(fr), aLabels(nullptr), _commentLine(fr._commentLine)
{
	if(size() != fr.size()) 
		aLabels = new string[size()];
	
	std::copy(fr.aLabels, fr.aLabels+size(), aLabels);
}
xyzFrame::xyzFrame(xyzFrame&& fr) noexcept: tvec(std::move(fr)), aLabels(fr.aLabels), 
								   _commentLine(std::move(fr._commentLine))
{
	fr.aLabels = nullptr;
}
xyzFrame::~xyzFrame()
{
	if(aLabels) delete[] aLabels;
}
xyzFrame& xyzFrame::operator=(const xyzFrame& fr)
{
	tvec::operator=(fr);
	
	aLabels = new string[size()];
	
	std::copy(fr.aLabels, fr.aLabels+size(), aLabels);

	_commentLine = fr._commentLine;
	
	return *this;
}
xyzFrame& xyzFrame::operator=(xyzFrame&& fr) noexcept
{
	tvec::operator=(fr);
	
	if(aLabels) 
		delete[] aLabels;
	aLabels = fr.aLabels;
	fr.aLabels = nullptr;
	
	_commentLine = std::move(fr._commentLine);
	
	return *this;
}

void xyzFrame::open(const string& fileName)
{
	ifstream file(fileName.c_str());
	if(!file) 
		throw system_error(error_code(static_cast<int>(errc::no_such_file_or_directory),system_category()));

	file>>*this;

	file.close();
}

void xyzFrame::write(const string& fileName)
{
	if(empty())
		throw out_of_range("xyzFrame::write(...): empty frame!");

	ofstream file(fileName.c_str());

	if(!file)
		throw system_error(error_code(static_cast<int>(errc::no_such_file_or_directory),system_category()));

	file<<*this;

	file.close();
}

ostream& xyzFrame::print(ostream& str) const
{
	str<<fixed<<setw(6)<<size()<<endl<<_commentLine<<endl;

	char buf[56];
	for(size_t i = 0; i < size(); ++i)
	{
		snprintf(buf, 56, "%-3s %14.8f %14.8f %14.8f\n", aLabels[i].c_str(), x(i), y(i), z(i));
		str<<buf;
	}

	return str;
}

istream& xyzFrame::read(istream& str)
{
	size_t s;
	str>>s;
	
	if(s == 0)
		throw out_of_range("xyzFrame::read(...): empty frame!");
	
	str.ignore(numeric_limits<streamsize>::max(), '\n');
	getline(str, _commentLine);

	tvec::resize(s);
	aLabels = new string[s];

	for(size_t i = 0; i < s; ++i)
	{
		str>>aLabels[i]>>x(i)>>y(i)>>z(i);
	}

	return str;
}

vxyzFrame::vxyzFrame(): xyzFrame(), vvec()
{

}
vxyzFrame::vxyzFrame(const string& fileName): xyzFrame(), vvec()
{
	open(fileName);
}
vxyzFrame::vxyzFrame(const xyzFrame& fr): xyzFrame(fr), vvec( std::move(tvec(fr.size())) )
{

}
vxyzFrame::vxyzFrame(const xyzFrame& fr, const tvec& v): xyzFrame(fr), vvec(v)
{
	
}
vxyzFrame::vxyzFrame(const vxyzFrame& fr): xyzFrame(fr), vvec(fr.vvec)
{

}
vxyzFrame::vxyzFrame(xyzFrame&&  fr) noexcept: xyzFrame(std::move(fr)), vvec( std::move(tvec(fr.size())) )
{

}
vxyzFrame::vxyzFrame(vxyzFrame&& fr) noexcept: xyzFrame(std::move(fr)), vvec( std::move(fr.vvec) )
{

}
vxyzFrame& vxyzFrame::operator=(const vxyzFrame& fr)
{
	vvec = fr.vvec;
	xyzFrame::operator=(fr);

	return *this;
}
vxyzFrame& vxyzFrame::operator=(vxyzFrame&& fr) noexcept
{
	vvec = std::move(fr.vvec);
	xyzFrame::operator=(fr);
	
	return *this;
}
vxyzFrame& vxyzFrame::operator=(const xyzFrame& fr)
{
	vvec.resize(fr.size());
	xyzFrame::operator=(fr);

	return *this;
}
vxyzFrame& vxyzFrame::operator=(xyzFrame&& fr) noexcept
{
	vvec.resize(fr.size());
	xyzFrame::operator=(fr);
	
	return *this;
}

ostream& vxyzFrame::print(ostream& str) const
{
	str<<fixed<<setw(6)<<size()<<endl<<_commentLine<<endl;

	char buf[80];
	for(size_t i = 0; i < size(); ++i)
	{
		snprintf(buf, 80, "%-3s %11.7f %11.7f %11.7f %11.7f %11.7f %11.7f\n", 
			aLabels[i].c_str(), x(i), y(i), z(i), vvec.x(i), vvec.y(i), vvec.z(i));
		str<<buf;
	}

	return str;
}

istream& vxyzFrame::read(istream& str)
{
	size_t s;
	str>>s;
	if(s == 0)
		throw out_of_range("vxyzFrame::read(...): empty frame!");
	
	str.ignore(numeric_limits<streamsize>::max(), '\n');
	getline(str, _commentLine);

	tvec::resize(s);
	vvec.resize(s);
	aLabels = new string[s];

	for(size_t i = 0; i < s; ++i)
	{
		str>>aLabels[i]>>x(i)>>y(i)>>z(i)>>vvec.x(i)>>vvec.y(i)>>vvec.z(i);
	}

	return str;
}
