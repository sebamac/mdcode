#include "rw_conf.hpp"
energyWriter::energyWriter(const string& s, const bool& append, const size_t &lines): 
						bufferedWriter(lines, s, "#Time    en    temp", append) 
{

}
energyWriter& energyWriter::write(const float& time, const double& en, const float& temp)
{
	//time  en temp
	snprintf(_buffer, 48, "%9.3e %13.8f %7.2f\n", time, en, temp);

	bufferedWriter::operator<<(_buffer);

	return *this;
}

trajWriter::trajWriter(const size_t& nat, const string& f, const bool& append): bufferedWriter(kFrame*(nat+1), f, append)
{
	
}

trajWriter& trajWriter::writeFrame(const xyzFrame& fr, const char* comment)
{
	snprintf(_buffer, 65, "%6lu\n%s\n", fr.size(), comment);
	bufferedWriter::operator<<(_buffer);

	for(size_t i = 0; i < fr.size(); ++i)
	{
		snprintf(_buffer, 65, "%-3s %12.8f %12.8f %12.8f\n", fr.atomLabel(i).c_str(), fr.x(i), fr.y(i), fr.z(i));
		bufferedWriter::operator<<(_buffer);
	}

	return *this;
}

trajVWriter::trajVWriter(const size_t& nat, const string& f, const bool& append): bufferedWriter(kFrame*(nat+1), f, append)
{
	
}

trajVWriter& trajVWriter::writeFrame(const vxyzFrame& fr, const char* comment)
{
	snprintf(_buffer, 80, "%6lu\n%s\n", fr.size(), comment);
	bufferedWriter::operator<<(_buffer);

	for(size_t i = 0; i < fr.size(); ++i)
	{
		snprintf(_buffer, 80, "%-3s %11.7f %11.7f %11.7f %11.7f %11.7f %11.7f\n", fr.atomLabel(i).c_str(), 
			fr[i][0], fr[i][1], fr[i][2],
			fr.velocities()[i][0], fr.velocities()[i][1],fr.velocities()[i][2]);
		bufferedWriter::operator<<(_buffer);
	}

	return *this;
}
