#include "logger.hpp"

using namespace std;

Logger::Logger()  noexcept: _filePath(""), _buffer("")
{
	_logger.exceptions(fstream::failbit | fstream::badbit);
}
// Logger::Logger(const char *path, ios_base::openmode mode)
// {
// 	_logger.exceptions(fstream::failbit | fstream::badbit);
	
// 	open(path, mode);
// }
// Logger::Logger(const string& path, ios_base::openmode mode)
// {
// 	_logger.exceptions(fstream::failbit | fstream::badbit);
	
// 	open(path.c_str(), mode);
// }
// Logger::Logger(const Logger& log): _filePath(log._filePath),
// 								   _buffer(log._buffer.str())
// {
//
// }

Logger& Logger::open(const char *path, ios_base::openmode mode)
{
	try
	{
		_logger.open(path, mode);
		_filePath = string(path);
		
		ifstream infile(path);
		if(infile.peek() == EOF) 
			_logger<<"Log file'"<<_filePath<<"' started at: "<<currentTime()<<std::endl;
		infile.close();
	} catch(fstream::failure bla)
	{
		throw fstream::failure("Logger::open(...): unable to open/create file");
	}
	
	return *this;
}

Logger& Logger::clear()
{
	if(_logger.is_open()) _logger.close();
	_logger.clear();
	_logger.open(_filePath.c_str(), ios_base::trunc);
	_logger<<"Log file'"<<_filePath<<"' started at: "<<currentTime()<<std::endl;
	
	_buffer.str("");
	_buffer.clear();
	
	return *this;
}

Logger& Logger::close()
{
	if(_logger.is_open())
	{
		_logger.flush();
		_logger<<"\nLog file'"<<_filePath<<"' closed at: "<<currentTime()<<std::endl<<std::endl<<std::flush;
		_logger.close();
	} else
	{
		throw fstream::failure("Logger::close() error: '"+_filePath+"' is not open!");
	}
	
	_buffer.str("");
	_buffer.clear();
	
	return *this;
}

Logger& Logger::flush()
{
	try
	{
		_logger<<_buffer.str()<<std::flush;
	} catch(std::fstream::failure &bla)
	{
		throw fstream::failure("Logger::flush() write error");
	}
	_buffer.str("");
	_buffer.clear();
	
	return *this;
}

string Logger::currentTime()
{
	system_clock::time_point p = system_clock::now();

	std::time_t t = system_clock::to_time_t(p);
	ostringstream str;
	str<<std::ctime(&t); // for example : Tue Sep 27 14:21:13 2011

	return str.str();
}
